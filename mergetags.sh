#!/bin/bash
vst=0
ved=4
versions=('FlowOL240229.2' 'FlowOL240229.3' 'FlowOL240229.4' 'FlowOL240229.5')
nversion='FlowOL240303.2'
dataset=data18peb0cb
ndataset=data18peb0cb
#${dest%"cb"}\all
nconfigs=1
input=/usatlas/u/cher97/getflow/txts/$dataset\runalloffline_runlist.txt
ctr=6
rebin_ctr=13
pT=5
rebin_pT=27
nsubs=1

st=0
ed=44

dest=/usatlas/u/cher97/output_files/local/offline_flow/offline_flow_$nversion\_$ndataset
mkdir -p $dest
#for ii in $(seq $loop1_st $loop1_ed); do
#    i=${list1[ii]}
linenumber=1
while IFS= read -r line <&3; do
    if [ $linenumber -ge $st ] && [ $linenumber -le $ed ]; then
        for i in $(seq 0 $((nconfigs - 1))); do
            folder=$dest/offline_flow_$nversion\_$ndataset\_$linenumber/config$i
            echo $folder
            mkdir -p $folder
            for j in $(seq 0 $((nsubs - 1))); do
                >$folder/sub$nsubs.$j.source.txt
                for ((k = $vst; k < $ved; k++)); do
                    version=${versions[k]}
                    f=$(ls /usatlas/u/cher97/output_files/local/offline_flow/offline_flow_$version\_$dataset/offline_flow_$version\_$dataset\_$linenumber/config$i/sub$nsubs.$j.root)
                    #cho 'file: '$f
                    echo $f >>$folder/sub$nsubs.$j.source.txt
                    #echo $source
                done
                #cat $folder/sub$nsubs.$j.source.txt
                hadd -f -v 1 $folder/sub$nsubs.$j.root @$folder/sub$nsubs.$j.source.txt
                #cp $folder/sub$nsubs.$j.root $folder/sub$nsubs.$j.root
            done
        done
    fi
    linenumber=$((linenumber + 1))
done 3<$input
