#!/bin/bash
#./organizeENloop.sh 1 $folder (spmethod) 2 $tag 3 $dataset 4$nconfig
origin=/atlasgpfs01/usatlas/data/cher97/$1/

input=/usatlas/u/cher97/getflow/txts/$3runall_runlist.txt
linenumber=1
nconfigs=$4
while IFS= read -r line <&3; do
    runnumber=${line#data18_hi.00}
    runnumber=${runnumber:0:6}
    echo $runnumber
    for i in $(seq 0 $((nconfigs - 1))); do
        echo config$i
        for j in $(seq 0 80); do
            dest=/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3.csv/$runnumber
            mkdir -p $dest
            cd $dest
            #echo $dest
            tail -n +2 -q $origin/$1_$2_$3_$linenumber/config$i/sub1.0/*.csv/eventnumber$j.csv >>eventnumber$j.csv
            #cat $origin/$1_$2_$3_$linenumber/config$i/sub1.0/*.csv/eventnumber$j.csv >>eventnumber$j.csv
        done
    done
    linenumber=$((linenumber + 1))
done 3<$input
