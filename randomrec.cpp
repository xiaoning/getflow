void randomrec()
{
    TRandom3 a = TRandom3();
    TGraph *b = new TGraph(100);
    TH1F *ntrks = new TH1F("ntrks", "Histograms of Mean N_{trk}", 100, 0, 1);
    TH1F *meanwt = new TH1F("meanwt", "Histogram of Mean w_{i}", 100, 0, 2000);
    for (int i = 0; i < 100; i++)
    {
        double tot = 0.;
        double totrec = 0.;
        for (int j = 0; j < 64; j++)
        {
            double tmp = a.Uniform(1);
            tot += tmp;
            totrec += 1. / tmp;
        }
        tot /= 64.;
        totrec /= 64.;
        b->SetPoint(i, tot, totrec);
        ntrks->Fill(tot);
        meanwt->Fill(totrec);
    }
    //gStyle->SetOptStat(0);
    TCanvas *c = new TCanvas("c", "c", 600, 500);
    b->GetXaxis()->SetTitle("Mean Simulated N_{trk}");
    b->GetYaxis()->SetTitle("Mean weight");
    b->SetMarkerSize(1);
    b->SetMarkerStyle(21);
    b->Draw("AP");
    c->SaveAs("weightsim.png");

    ntrks->GetXaxis()->SetTitle("Mean Simulated N_{trk}");
    ntrks->GetYaxis()->SetTitle("Counts");
    ntrks->Draw("HIST");
    c->SaveAs("weightsimntrks.png");

    meanwt->GetXaxis()->SetTitle("Mean Simulated N_{trk}");
    meanwt->GetYaxis()->SetTitle("Counts");
    meanwt->Draw("HIST");
    c->SetLogy();
    c->SaveAs("weightsimmeanwt.png");
}