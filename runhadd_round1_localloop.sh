#!/bin/bash
#input="/atlasgpfs01/usatlas/data/cher97/mc16_5TeV.txt"
# ./runallloop.sh no1 process# no2 inputfile location no3 timestamp
# for large dataset, use pnfs for storage

linenumber=0

while IFS= read -r line <&3; do
        if [ $1 -eq $linenumber ]; then
                dest=${line%/*}
                cd $dest
                mkdir -p $3$linenumber
                cd $3$linenumber
                echo 'pwd'$PWD
                hadd -v 1 -f $dest/$3$linenumber/round1_$linenumber.root @$line

                while IFS= read -r line_single <&2; do
                        find $line_single -size 0 >$dest/$3$linenumber/$linenumber'_size0.txt'
                done 2<$line
        fi
linenumber=$((linenumber+1))
done 3<$2
