#!/bin/bash
#./submitrerun.sh no1:location no2:MCE211203.1 no3:mc18jet no4: optional, subsampling no5:optional combine m files no6:optional n configs no7: optional partial no8:optional use pnfs
#copy and open to be edited steering macro
home=/usatlas/u/cher97
master=/atlasgpfs01/usatlas/data/cher97/output_files/local/
dest=$master/$1/$1\_$2\_$3

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    comb=1
else
    comb=$5
fi

if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi

if [ "$7" == "" ]; then
    partial=1
else
    partial=$7
fi

dataset=$3
if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi
for ((j = 0; j < $configs; j++)); do
    echo 'config '$j
    cd /usatlas/scratch/cher97/$1/$1\_$2$8\_$3$2$8/$1\_$2$8\_$3$2$8\_1/config$j/
    echo /usatlas/scratch/cher97/$1/$1\_$2$8\_$3$2$8/$1\_$2$8\_$3$2$8\_1/config$j/
    for ((n = 0; n < $nsubs; n++)); do
        cd sub$nsubs\.$n
        for i in *.root; do
            mv "$i" "rerun$8${i}"
            #echo "$i"to"rerun$8${i}"
            cp "rerun$8${i}" /usatlas/scratch/cher97/$1/$1\_$2\_$3/$1\_$2\_$3\_1/config$j/sub$nsubs\.$n/
#cp "${i}" /usatlas/scratch/cher97/$1/$1\_$2\_$3/$1\_$2\_$3\_1/config$j/sub$nsubs\.$n/        
done
        cd ..
    done
done

cd $home/getflow
