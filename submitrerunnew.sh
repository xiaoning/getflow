#!/bin/bash
#./submitrerun.sh no1:location no2:MCE211203.1 no3:mc18jet no4: optional, subsampling no5:optional combine m files no6:optional n configs no7: optional partial no8:# of rerun
#copy and open to be edited steering macro
home=/usatlas/u/cher97
master=/atlasgpfs01/usatlas/data/cher97/output_files/local/
curr=$8
prev=$((curr - 1))
newtag=$2$curr
newdataset=$3$2$curr
rerunname=$3$2$curr'rerun'
rerunlistname=$3$2$curr'runalloffline_runlist'

if [ "$curr" == "0" ]; then
    echo "First rerun"
    dest=$master/$1/$1\_$2\_$3
    dataset=$3
    prevrerunlistname=$3'runalloffline_runlist'
else
    echo $8' rerun'
    dest=$master/$1/$1\_$2$prev\_$3$2$prev
    dataset=$3$2$prev
    prevrerunlistname=$3$2$prev'runalloffline_runlist'
fi

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    comb=1
else
    comb=$5
fi

if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi

if [ "$7" == "" ]; then
    partial=1
else
    partial=$7
fi

desterr=$dest/err.tmp
>$desterr
for ((j = 0; j < 1; j++)); do
    cd $dest/config$j
    sed -n "/in.root is truncated/p" err.xrdcp >>$desterr
    sed -n "/in.root does not exist/p" err.xrdcp >>$desterr
done

sed -i "s/Error in <TFile::Init>: file \/tmp\/.*$dataset\_//" $desterr
sed -i "s/\/in.root is truncated.*recover//" $desterr
sed -i "s/Error in <TFile::TFile>: file \/tmp\/.*$dataset\_//" $desterr
sed -i "s/\/in.root does not exist//" $desterr
sed -i "s/.*non-null arguments.//" $desterr
sed -i "/^$/d" $desterr
sed -i 's/_.*_/_/' $desterr

echo $rerunname >/usatlas/u/cher97/getflow/txts/$rerunlistname.txt
>/usatlas/u/cher97/getflow/txts/$rerunname.txt
while IFS= read -r line <&3; do
    runnum=${line%'_'*}
    linenumber=$((${line#*'_'} + 1))
    runlist=$(sed "${runnum}q;d" /usatlas/u/cher97/getflow/txts/$prevrerunlistname.txt)
    input='/usatlas/u/cher97/getflow/txts/'$runlist'.txt'
    sed "${linenumber}q;d" $input >>/usatlas/u/cher97/getflow/txts/$rerunname.txt
done 3<$desterr

sed -i '$!N; /^\(.*\)\n\1$/!P; D' /usatlas/u/cher97/getflow/txts/$rerunname.txt
#awk '!_[$0]++' /usatlas/u/cher97/getflow/txts/$rerunname.txt

echo "file list"
cat /usatlas/u/cher97/getflow/txts/$rerunname.txt
cd $home/getflow

#exit

echo "./runalloffline.sh $1 $newtag $newdataset $4 $5 $6 $7 data cher97 0 1"
./runalloffline.sh $1 $newtag $newdataset $4 $5 $6 $7 data cher97 0 1
