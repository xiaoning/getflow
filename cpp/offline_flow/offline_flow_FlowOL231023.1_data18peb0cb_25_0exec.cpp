#include <TROOT.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <dirent.h>
#include "TFile.h"
#include "TChain.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TApplication.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TSystem.h"
#include <algorithm>
#include "TStyle.h"
#include "TColor.h"
#include "TProfile.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include <math.h>

#include <TH2.h>
#include <TStyle.h>

/*#include "../atlasstyle-00-04-02/AtlasUtils.h"
#include "../atlasstyle-00-04-02/AtlasStyle.h"
#include "../atlasstyle-00-04-02/AtlasLabels.h"
#include "../atlasstyle-00-04-02/AtlasStyle.C"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "../atlasstyle-00-04-02/AtlasLabels.C"
#include "../atlasstyle-00-04-02/AtlasUtils.C"
#endif

#ifdef __CINT__
gROOT->LoadMacro("../atlasstyle-00-04-02/AtlasLabels.C");
gROOT->LoadMacro("../atlasstyle-00-04-02/AtlasUtils.C");
#endif
*/

#include "/usatlas/u/cher97/offline_flow/Flow.h"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "/usatlas/u/cher97/offline_flow/Flow_main.cpp"
#include "/usatlas/u/cher97/offline_flow/Flow.cpp"
#include "/usatlas/u/cher97/offline_flow/corr.cpp"
#include "/usatlas/u/cher97/offline_flow/inits.cpp"
#include "/usatlas/u/cher97/offline_flow/performances.cpp"
#include "/usatlas/u/cher97/offline_flow/Utils.cpp"
#include "/usatlas/u/cher97/offline_flow/qn.cpp"
#endif

#ifdef __CINT__
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/Flow_main.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/Flow.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/corr.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/inits.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/performances.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/Utils.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/qn.cpp");
#endif

const std::vector<float> pT_range_config = cms_pT_range_small;
const std::vector<float> ctr_range_config = cms_ctr_range;
const std::vector<int> N_config = {4};
const std::vector<int> Qn_N_config = {2, 3, 4, 6, 8};
const int nsubs_config = 1;
const int datatype_id = 0;
const int m_weight_tag = 1; // 0 for uniform weight, 1 for actual weight, 2 for random
const TString m_version = "FlowOL231019.1";
const TString m_dataset = "data18peb0";
const int linenumber = 32;
const int config = 0;
const TString m_dest = "/atlasgpfs01/usatlas/data/cher97";
const TString m_wtversion = "FlowOL230914.1";
const TString m_rcversion = "FlowOL231016.1";
const int m_refeta_id = 0;
const int m_poieta_id = 0;
const double m_refptlow = 0.5;
const double m_refpthigh = 3.0;
const bool m_of = true;
const bool m_perf = true;
const bool m_corr = false;
const bool m_qn = false;
const bool m_rc = false;
const bool m_event = false;
const bool m_cov = false;
const std::vector<bool> selections_config = {0, 1, 1, 1}; //"both hits in inner most tracker","d0 pull cut 3.0", "z0sintheta pull cut 3.0", "HITight"

void exec(std::string in, std::string out)
{
    gInterpreter->GenerateDictionary("vector<vector<float> >", "vector");
    gInterpreter->GenerateDictionary("vector<vector<int> >", "vector");
    gInterpreter->GenerateDictionary("vector<vector<double> >", "vector");

    std::cout << "Entering main" << std::endl;
    // make object
    Flow *flow = new Flow(in, out, datatype_id, 0, nsubs_config);
    // performance
    flow->setPerformance(m_perf);
    // Qn
    flow->setQn(m_qn);
    flow->setNsQn(Qn_N_config);

    // correlators
    flow->setCorr(m_corr);
    flow->setNs_corr(N_config);
    flow->setOverflow(m_of);
    flow->setCov(m_cov);
    // recentering
    flow->setRC(m_rc);
    flow->setRcVersion(m_rcversion);
    // weight
    flow->setWeightTag(m_weight_tag);
    flow->setWtVersion(m_wtversion);
    flow->setTrkCorrection(true, false);
    flow->setMCCorrection(true);
    flow->setPrescaleCorrection(true);
    // event filter
    flow->setEventFilter(false);
    // kinematics
    flow->setRefPt(m_refptlow, m_refpthigh);
    flow->setRefEta(m_refeta_id);
    flow->setPoiEta(m_poieta_id);
    flow->setpTRange(pT_range_config);
    flow->setctrRange(ctr_range_config);

    // event detail
    flow->setDoEvent(m_event);
    flow->setStopAtN(100);
    flow->setStopAtNDirectory(Form("%s/offline_flow/offline_flow_%s_%s_%d/config%d/csv/", m_dest.Data(), m_version.Data(), m_dataset.Data(), linenumber, config));

    // run configs
    //flow->setNMaxEvents(30000);
    flow->setVerbose(0);

    // Run
    flow->initBranches();
    flow->initTrackSelections(selections_config);
    flow->Loop();
    // init output path
    // init histogram
    // fill histogram (loop)
    // output
    return;
}
