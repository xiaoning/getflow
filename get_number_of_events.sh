>txts/$1_nevents.csv
while IFS= read -r line <&3; do
    tag=$(rucio list-files $line | tail -1)
    echo $line','${tag#*'Total events : '} >>txts/$1_nevents.csv
done 3<txts/$1.txt
