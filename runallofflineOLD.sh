#!/bin/bash
home=/usatlas/u/cher97
input=$home/getflow/txts/$3runalloffline_runlist.txt
raccount

if [ "$8" == "" ]; then
    dest='/atlasgpfs01/usatlas/data/cher97'
else
    dest='/pnfs/usatlas.bnl.gov/users/cher97'
fi

echo $dest

#./runalloffline.sh no1:location no2:MCE211203.1 no3:mc18jet no4: optional, subsampling no5:optional combine m files no6:optional n configs no7: optional partial no8:optional use pnfs
#copy and open to be edited steering macro
mkdir -p $home/getflow/cpp/$1
mkdir -p $home/getflow/condors/$1
mkdir -p $dest/$1

cd $home/$1
git add -A
git commit -m "commit for running$2"
git tag -d $2
git push --delete origin $2
git tag -a $2 -m "$2"
git push --tags

template=$1_$2_$3
linenumber=1
st=0
ed=44

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    comb=1
else
    comb=$5
fi

if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi

if [ "$7" == "" ]; then
    partial=1
else
    partial=$7
fi

for ((j = 0; j < $configs; j++)); do
    cp $home/$1/exec.cpp $home/getflow/cpp/$1/$template'_'$j'exec.cpp'
    sed -i "s@^const int nsubs_config.*@const int nsubs_config = $nsubs;@" $home/getflow/cpp/$1/$template'_'$j'exec.cpp'
    vim $home/getflow/cpp/$1/$template'_'$j'exec.cpp'
done

while IFS= read -r line <&3; do
    echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
    folder=$1/$1_$2_$3_$linenumber

    if [ "$line" = "" ] || [ "$line" = "*done*" ]; then
        break
    fi

    if [ $linenumber -gt $st ] && [ $linenumber -le $ed ]; then
        for ((j = 0; j < $configs; j++)); do
            cp $home/getflow/cpp/$1/$template'_'$j'exec.cpp' $home/getflow/cpp/$folder'_'$j'exec.cpp'
            sed -i "s@^.const TString m_version.*@const TString m_version = \"$2\";@" $home/getflow/cpp/$folder'_'$j'exec.cpp'
            sed -i "s@^.const int linenumber.*@const int linenumber = $linenumber;@" $home/getflow/cpp/$folder'_'$j'exec.cpp'
        done
        #JZ=-1
        #if [[ "$3" = *"mc"*"jet"* ]]; then
        #    tmps=${line#*JZ}
        #    JZ=${tmps:0:1}
        #    sed -i "s@.*alg.JZ =.*@alg.JZ = $JZ@" $home/getflow/py/$folder'ATestRun_eljob.py'
        #fi

        if [ "$line" = "" ]; then
            break
        fi

        cp $home/getflow/condors/run_temp.job $home/getflow/condors/$folder'_runalloffline.job'

        sed -i "s@^Output.*@Output       = $dest/$folder/logFiles/out.\$(Process)@" $home/getflow/condors/$folder'_runalloffline.job'
        sed -i "s@^Error.*@Error       = $dest/$folder/logFiles/err.\$(Process)@" $home/getflow/condors/$folder'_runalloffline.job'
        sed -i "s@^Log.*@Log       = $dest/$folder/logFiles/log.\$(Process)@" $home/getflow/condors/$folder'_runalloffline.job'
        sed -i "s@^Executable.*@Executable   = /usatlas/u/cher97/getflow/runallofflineloop.sh@" $home/getflow/condors/$folder'_runalloffline.job'
        sed -i "s@^Arguments.*@Arguments       = \$(Process) $folder $line $nsubs $comb $configs $dest $home@" $home/getflow/condors/$folder'_runalloffline.job'
        nofful=$(wc -l <$home/getflow/txts/$line.txt)
        nof=$(((nofful + comb - 1) / comb))
        nof=$((nof / $partial))
        sed -i "s@^Queue.*@Queue $nof@" $home/getflow/condors/$folder'_runalloffline.job'

        #exec 0<&1

        #read -p "Do you wish to remove old folder and rerun? (y/n) " answer
        #if [[ $answer =~ ^[Yy]$ ]]; then
        echo "deleting original run folder"
        rm -rf $dest/$folder/
        #else
        #    echo "writing to old folder"
        #fi

        mkdir -p $dest/$folder/
        mkdir -p $dest/$folder/logFiles/

        #cat $home/getflow/condors/$folder'_runall.job'
        #cat $home/getflow/condors/$folder'_runalloffline.job'
        condor_submit $home/getflow/condors/$folder'_runalloffline.job'
    fi
    linenumber=$((linenumber + 1))
done 3<$input
