import pandas as pd
import shutil
import re
import subprocess


label = "SPMethod230123.1"
data = "data18mb"
fcals = [0]

indata = f"{data}"
outname = f"{data}"
listname = f"{data}_combined"
fcalnames = ["", "_halfouter", "_halfinner"]

PCCC = True
save = False
saveold = False
copyover = False


df = pd.read_csv(f"txts/{indata}runall_runlist.txt",
                 header=None, names=["dataset"])
for fcal in fcals:
    fcalname = fcalnames[fcal]
    if (PCCC):
        runsdict = {}
        for index, row in df.iterrows():
            dataset = row['dataset']
            print(dataset)
            if (dataset == 'done'):
                continue
            run = re.split('\.', dataset)[1][2:]
            prod = re.split('\.', dataset)[-1]
            stream = re.split('\.', dataset)[2]
            if (run not in runsdict):
                runsdict[run] = ["", "", "", ""]
            i = "physics_CC" in stream
            print(i)
            runsdict[run][i] = f"../output_files/local/spmethod/spmethod_{label}_{indata}_{index+1}/config{fcal}/sub1.0.root"
            runsdict[run][2] = prod.replace('_p4404', '')
            if (save):
                subprocess.run(["cp", "-rf", f"../output_files/local/spmethod/spmethod_{label}_{indata}_{index+1}",
                                f"../output_files/local/spmethod/spmethod_{label}_save_{indata}_{index+1}"])
        runs = 0
        for run in runsdict:
            print(run)
            print(f"PC{runsdict[run][0]}")
            print(f"CC{runsdict[run][1]}")
            if (runsdict[run][0] == "" or runsdict[run][1] == ""):
                continue
            else:
                runs += 1
                subprocess.run(
                    ["mkdir", "-p", f"../output_files/local/spmethod/spmethod_{label}_{listname}_{run}/config{fcal}"])
                subprocess.run(
                    ["hadd", "-f", f"../output_files/local/spmethod/spmethod_{label}_{listname}_{run}/config{fcal}/sub1.0.root", runsdict[run][0], runsdict[run][1]])
                if (copyover):
                    subprocess.run(["mv", f"../spmethod/source/spmethod/data/qmean.{outname}{fcalname}.{run}.{runsdict[run][2]}.tot.root",
                                    f"../spmethod/source/spmethod/data/qmean.{outname}{fcalname}.{run}.{runsdict[run][2]}.totold.root"])
                    subprocess.run(
                        ["hadd", "-f", f"../spmethod/source/spmethod/data/qmean.{outname}{fcalname}.{run}.{runsdict[run][2]}.tot.root", runsdict[run][0], runsdict[run][1]])
        print(runs)
    else:
        for index, row in df.iterrows():
            dataset = row['dataset']
            print(dataset)
            run = re.split('\.', dataset)[1][2:]
            prod = re.split('\.', dataset)[-1]
            #stream = re.spit('\.', dataset)[2]
            print(run)
            print(prod)
            if (saveold):
                subprocess.run(["mv", f"../spmethod/source/spmethod/data/qmean.{outname}{fcalname}.{run}.{prod}.tot.root",
                                f"../spmethod/source/spmethod/data/qmean.{outname}{fcalname}.{run}.{prod}.totold.root"])
            shutil.copy(
                f"../output_files/local/spmethod/spmethod_{label}_{indata}_{index+1}/config{fcal}/sub1.0.root", f"../spmethod/source/spmethod/data/qmean.{outname}{fcalname}.{run}.{prod}.tot.root")
