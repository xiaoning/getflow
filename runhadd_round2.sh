#!/bin/bash
#./runhadd_round2.sh $1 foldername:spmethod $2 tag $3 data $4:nsubs $5:timestamp $6: nconfigs
home=/usatlas/u/cher97
input=$home/getflow/txts/$3runall_runlist.txt
dest=/atlasgpfs01/usatlas/data/cher97/
master=$dest
master='/usatlas/scratch/cher97'
linenumber=1
nsubs=$4
echo $nsubs
nconfigs=$6
st=0
ed=44

for j in $(seq 0 $((nconfigs - 1))); do
    mkdir -p $dest/output_files/local/$1/$1\_$2\_$3/config$j/cpp/
    #cp -r $master/$1/$1\_$2\_$3/config$j/cpp/$1/ $dest/output_files/local/$1/$1\_$2\_$3/config$j/cpp/
done

while IFS= read -r line <&3; do
    echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
    folder=$1/$1\_$2\_$3/$1_$2_$3_$linenumber

    if [ "$line" = "" ] || [ "$line" = "*done*" ]; then
        break
    fi

    if [ $linenumber -gt $st ] && [ $linenumber -le $ed ]; then
        cp $home/getflow/condors/run_temp.job $home/getflow/condors/$folder'_runhadd_round2.job'

        sed -i "s@^Executable.*@Executable   = /usatlas/u/cher97/getflow/runhadd_round2loop.sh@" $home/getflow/condors/$folder'_runhadd_round2.job'
        sed -i "s@^Arguments.*@Arguments       = \$(Process) $master/$folder $nsubs $5 $6 $dest/output_files/local/$folder@" $home/getflow/condors/$folder'_runhadd_round2.job'
        sed -i "s@^Queue.*@Queue $nsubs@" $home/getflow/condors/$folder'_runhadd_round2.job'
        sed -i "s@^Output.*@Output       = $master/$folder/logFiles/outhadd2.\$(Process)@" $home/getflow/condors/$folder'_runhadd_round2.job'
        sed -i "s@^Error.*@Error       = $master/$folder/logFiles/errhadd2.\$(Process)@" $home/getflow/condors/$folder'_runhadd_round2.job'
        sed -i "s@^Log.*@Log       = $master/$folder/logFiles/loghadd2.\$(Process)@" $home/getflow/condors/$folder'_runhadd_round2.job'
        for j in $(seq 0 $((nconfigs - 1))); do
            mkdir -p $dest/output_files/local/$folder/config$j/
            #cp -r $master/$folder/config$j/cpp/ $dest/output_files/local/$folder/config$j/

        done
        #cp -r $master/$folder/logFiles/ $dest/output_files/local/$folder/
        condor_submit $home/getflow/condors/$folder'_runhadd_round2.job'
    fi
    linenumber=$((linenumber + 1))
done 3<$input
