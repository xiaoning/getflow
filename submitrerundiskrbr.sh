#!/bin/bash
#./submitrerun.sh no1:location no2:MCE211203.1 no3:mc18jet no4: optional, subsampling no5:optional combine m files no6:optional n configs no7: optional partial no8:optional use pnfs
#copy and open to be edited steering macro
home=/usatlas/u/cher97
master=/atlasgpfs01/usatlas/data/cher97/output_files/local/
dest=$master/$1/$1\_$2\_$3

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    comb=1
else
    comb=$5
fi

if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi

if [ "$7" == "" ]; then
    partial=1
else
    partial=$7
fi

if [ "$8" == "" ]; then
    st=0
else
    st=$8
fi

if [ "$9" == "" ]; then
    ed=0
else
    ed=$9
fi

dataset=$3
input=$home/getflow/txts/$3runalloffline_runlist.txt
j=0

linenumber=1
while IFS= read -r line <&3; do
    echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
    if [ $linenumber -gt $st ] && [ $linenumber -le $ed ]; then
        desterr=$dest/$1\_$2\_$3\_$linenumber/err.tmpd
        >$desterr
        for ((j = 0; j < $configs; j++)); do
            cd $dest/$1\_$2\_$3\_$linenumber/config$j
            sed -n "/Disk quota exceeded/p" err.allclean >>$desterr
        done
        sed -i "/cp\: failed to extend/d" $desterr
        sed -i "/line /d" $desterr
        sed -i "s/cp\: error writing ‘\/usatlas\/scratch\/.*\/$1\/$1\_$2\_$3\/$1\_$2\_$3\_//" $desterr
        sed -i "s/config$j\/sub$nsubs\.0\/flow\_//" $desterr
        sed -i "s/.$nsubs.0.root’: Disk quota exceeded//" $desterr
        sed -i 's![^-]*$!!' $desterr
        sed -i 's![^_]*$!!' $desterr
        sed -i "s/-//" $desterr
        sed -i "s/_//" $desterr
        sed -i "s/\//_/" $desterr
        sed -i "/^$/d" $desterr
        #cat err.tmpd

        #for i in $(seq 1 44); do
        #    sed -i "s/cp\: error writing ‘\/atlasgpfs01\/usatlas\/data\/cher97\/$1\/$1\_$2\_$3\/$1\_$2\_$3\_$i\/config$j\/sub$nsubs\.0\/flow\_//" err.tmpd
        #done

        #sed -i 's/_/,/' err.tmpd
        #cat err.tmp
        tmp=$(python3 $home/getflow/submitrerunrbr.py $1 $2 $3 $4 1 tmpd $linenumber)
        a=${tmp%'['*}
        a=${a%']'*}
        a=${a#*'['}
        #echo $a
        b=${tmp##*'['}
        b=${b%']'*}
        #echo $b
        sed -i "s/^runlist=(.*/runlist=($a)/" $home/getflow/runallofflineselect.sh
        sed -i "s/^list=(.*/list=($b)/" $home/getflow/runallofflineselect.sh
        vim $home/getflow/runallofflineselect.sh
        cd $home/getflow

        ./runallofflineselect.sh $1 $2 $3 $4 $5 $6
    fi
    linenumber=$((linenumber + 1))
done 3<$input
