#!/bin/bash
#./runhadd_round2.sh $1 foldername:spmethod $2 tag $3 data $4:nsubs $5:nconfigs
input=~/getflow/txts/$3runall_runlist.txt

linenumber=1

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    nconfigs=1
else
    nconfigs=$5
fi

while IFS= read -r line <&3; do
    echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
    for k in $(seq 0 $((nconfigs - 1))); do
        folder=$1/$1_$2_$3_$linenumber/config$k

        if [ "$line" = "" ]; then
            break
        fi

        cd /atlasgpfs01/usatlas/data/cher97/output_files/local/$folder/
        hadd -f tots.root sub$4.*.root
    done
    linenumber=$((linenumber + 1))
done 3<$input
