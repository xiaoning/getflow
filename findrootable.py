import pandas as pd
import sys


def findrootable(dataset: str):
    df = pd.read_csv(f'txts/{dataset}_pnfs.txt', sep="\||: |://", skiprows=3, header=None, engine='python')
    df.columns = ['null', 'scope', 'filename',
                  'size', 'code', 'disk', 'xrdspec', 'path', '']
    df = df[df['path'].notna()]
    dfnew = df[((df['path'].str.contains('/pnfs')) | (df['xrdspec']
                == 'root'))][['filename', 'path']].groupby(by='filename').max()
    dfout = dfnew['path'].apply(
        lambda x: f'root://{x}').reset_index(drop='True')
    dfout.to_csv(f'txts/{dataset}.txt', header=None, index=None)


if __name__ == '__main__':
    findrootable(sys.argv[1])
