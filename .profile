#+##########################################################################
# File:            .profile
# Description:     general purpose .profile file
#                  This file gives examples of how to modify the behaviour of
#                  Bourne-shells.
#                  It sources a file on the central file server which is
#                  starting the HEP scripts if they are not installed locally.
# Authors:         A. Taddei (CERN/CN/DCI)
# Site Contact:    Setup.Support@cern.ch
# Version: 	   3.1.1
# Last Change:	   10 January 1997
#-############################################################################

if [ -r /usr/local/lib/hepix/shells/hep/central_login.sh ]; then
    . /usr/local/lib/hepix/shells/hep/central_login.sh
fi

#======================================================================#
# The following is a set of examples.
#
# If you want to try them uncomment the line(s) you selected.
#======================================================================#
#
# If you want to have ENVIRONMENT specific settings:
#===================================================
# Template for environment dependent login
# case ${ENVIRONMENT} in
#        INTERACTIVE) # Interactive non-login behaviour of the shell
#                     # (No output)
#                     ;;
#        LOGIN)       # STANDARD LOGIN AND DMLOGIN :
#                     # echo "Environment $ENVIRONMENT"
#                     #
#                     # Warning quota limits:
#                     #======================
#                     # /usr/local/lib/hepix/tools/quota-warn
#                     #
#                     # News:
#                     #======
#                     ;;
#        X11)         # X11 LOGIN (No output):
#                     ;;
#        BATCH)       # NQS,... (No output):
#                     ;;
# esac
#
#
# How can you change your prompt:
#================================
# Definition		 	# Result	# Comment
#-------------------------------------------------------------------
# PS1="[$HOST] "  		# [plus1]	#
# PS1='R; ';		        # R;		#
#
#
#
# How can you change the path:
#=============================
# Basic rule:
#------------
# To include the access to new programs after the defaults:
# PATH=$PATH:new_directory; export PATH
#
# To include the access to new programs before the defaults:
# PATH=new_directory:$PATH; export PATH
#
# Examples of new_directory could be:
# new_directory			# Comment
#-----------------------------------------------------------------------
# /afs/cern.ch/user/f/fred/blysiin 	# To be able to execute Fred's programs
# /usr/local/bin/gnu		# To access GNU programs
#
# So the next lines are provided in order to help you
# removing the '.' in the path (if any)! (refer to CN/DCI/170 from the UCO)
#-----------------------------------------------
# PATH=`echo $PATH | sed 's/ \.//'`; export PATH
# PATH=`echo $PATH | sed 's/\. //'`; export PATH
#
#
# How can you set environment variables:
#======================================
# Action				# Comment
#--------------------------------------------------------------
# EDITOR=nedit; export EDITOR		# if you want nedit as default
# PRINTER=513-lps; export PRINTER  	# print on 513-lps as default
# LPDEST=513-ps; export LPDEST          # another way to set a printer
# LPDEST="$PRINTER"; export LPDEST      # if PRINTER is defined, use it!
# PRINT_CMD="xprint"; export PRINT_CMD  # The printing command mechanism
#                                       # used at CERN
# PAGER=less; export PAGER		# use less to read man pages
# FMHOME=/afs/cern.ch/@sys/usr/fm4; export FMHOME # to use Framemaker
#
#
# ORACLE
# ------
# If you want ORACLE uncomment the next lines:
#=============================================
# if [ -r /usr/local/lib/hepix/oracle_login.sh ]; then
#    . /usr/local/lib/hepix/oracle_login.sh
# fi
#

#EOF
export PROMPT_COMMAND="echo -n \[\$(date +%H:%M:%S)\]\ "
export ALRB_TutorialData=/afs/cern.ch/atlas/project/PAT/tutorial/cern-jan2018/
export RUCIO_ACCOUNT=xiaoning
export HOME_DIR=/usatlas/u/cher97
export LXPLUS_DIR=/afs/cern.ch/work/x/xiaoning
export TEST_SAMPLE=/atlasgpfs01/usatlas/data/cher97/mc16_5TeV/mc16_5TeV.420272.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2_bbfilter.recon.AOD.e7383_s3428_r11320/AOD.17946810._000002.pool.root.1

#setupATLAS
#cd /usatlas/u/cher97/dataFiles/
#asetup --restore
#source x86_64-*/setup.sh

export RUCIO_HOME="/cvmfs/atlas.cern.ch/repo/sw/ddm/rucio-clients/latest"
export RUCIO_AUTH_TYPE="x509_proxy"

export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
alias setupATLAS="source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
setupATLAS -2
lsetup "root 6.24"
#export ALRB_rootVersion=6.14.04-x86_64-slc6-gcc62-opt
#lsetup root
#cd /usatlas/u/cher97/dataFiles/

alias runsp='raccount; cd /usatlas/u/cher97/getflow/;./runsploop.sh Qmean210916.1 _pnfs 365678 PC 3 True 0.5'
alias rebuild='cd ../build; asetup AnalysisBase,21.2.110;cmake ../source; make; source x86_64-slc7-gcc8-opt/setup.sh; cd ../run'
function runlocal() {
    cd ../run
    rm -rf submitDir
    ../source/$1/share/ATestRun_eljob_local.py --submission-dir=submitDir
}

function runold() {
    cd ../run
    rm -rf submitDir
    ../source/$1/share/ATestRun_eljob.py --submission-dir=submitDir
}

function mergelog() {
    cd /atlasgpfs01/usatlas/data/cher97/
    nconfigs=$4
    for j in $(seq 0 $((nconfigs - 1))); do
        mkdir -p /atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config$j
        cd output_files/local/$1/$1_$2_$3/config$j
        if [ "$5" == "" ]; then
            st=1
        else
            st=$5
        fi
        if [ "$6" == "" ]; then
            ed=44
        else
            ed=$6
        fi
        >err.all
        >out.all
        >log.all
        >errhadd1.all
        >outhadd1.all
        >loghadd1.all
        >errhadd2.all
        >outhadd2.all
        >loghadd2.all
        for k in $(seq $st $ed); do
            cd /atlasgpfs01/usatlas/data/cher97/$1/$1_$2_$3/$1_$2_$3_$k/logFiles/
            nf=$(ls out.* | wc -l)
            for l in $(seq 0 $((nf - 1))); do
                cat out.$l >>/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config0/out.all
                cat err.$l >>/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config0/err.all
                cat log.$l >>/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config0/log.all
            done
            nf=$(ls outhadd1.* | wc -l)
            for l in $(seq 0 $((nf - 1))); do
                cat outhadd1.$l >>/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config0/outhadd1.all
                cat errhadd1.$l >>/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config0/errhadd1.all
                cat loghadd1.$l >>/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config0/loghadd1.all
            done
            nf=$(ls outhadd2.* | wc -l)
            for l in $(seq 0 $((nf - 1))); do
                cat outhadd2.$l >>/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config0/outhadd2.all
                cat errhadd2.$l >>/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config0/errhadd2.all
                cat loghadd2.$l >>/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config0/loghadd2.all
            done
        done
        cd /atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3/config$j
        cp err.all err.allclean
        sed -i '/\/s]/d' err.allclean
        cp err.allclean err.xrdcp
        sed -n -i '/[ERROR]/p' err.xrdcp
        sed -n -i '/^Error/p' err.xrdcp
        cp out.all out.allclean
        sed -n -i '/^\/pnfs\//p' out.allclean
        echo $(cmp out.allclean ~/getflow/txts/$3files.txt)
        if [ "$7" != "" ]; then
            /usatlas/u/cher97/getflow/submitrerun.sh $1 $2 $3 $7 $8 $4
        fi
    done
}

function mergelogscratch() {
    if [ "$9" == "" ]; then
        user=cher97
    else
        user=$9
    fi
    master=/atlasgpfs01/usatlas/data/cher97/
    origin=/usatlas/scratch/$user/
    cd $master
    nconfigs=$4
    for j in $(seq 0 $((nconfigs - 1))); do
        echo 'Config '$j
        mkdir -p $master/output_files/local/$1/$1_$2_$3/config$j
        cd $master/output_files/local/$1/$1_$2_$3/config$j
        if [ "$5" == "" ]; then
            st=1
        else
            st=$5
        fi
        if [ "$6" == "" ]; then
            ed=44
        else
            ed=$6
        fi
        >err.all
        >out.all
        >log.all
        >errhadd1.all
        >outhadd1.all
        >loghadd1.all
        >errhadd2.all
        >outhadd2.all
        >loghadd2.all
        for k in $(seq $st $ed); do
            echo 'Run ln'$k
            mkdir -p $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j
            cd $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j
            >err.all
            >out.all
            >log.all
            >errhadd1.all
            >outhadd1.all
            >loghadd1.all
            >errhadd2.all
            >outhadd2.all
            >loghadd2.all
            cd $origin/$1/$1_$2_$3/$1_$2_$3_$k/logFiles/
            nf=$(ls out.* | wc -l)
            if [ "${10}" == "0" ]; then
                for l in $(seq 0 $((nf - 1))); do
                    if [ $(($l % 50)) -eq 0 ]; then
                        echo "Processing file log raw "$l"/"$nf
                    fi
                    sed -n '/^\/pnfs\//p' out.$l >>$master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/out.all
                    sed -n '/\/s]/!s/^//p' err.$l >>$master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/err.all
                    cat log.$l >>$master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/log.all
                done
                cat $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/out.all >>$master/output_files/local/$1/$1_$2_$3/config$j/out.all
                cat $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/err.all >>$master/output_files/local/$1/$1_$2_$3/config$j/err.all
                cat $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/log.all >>$master/output_files/local/$1/$1_$2_$3/config$j/log.all
            fi
            if [ "${10}" == "1" ]; then
                nf=$(ls outhadd1.* | wc -l)/$nconfigs
                echo 'Range: '$((j * nf))' to '$(((j + 1) * nf))
                for l in $(seq $((j * nf)) $(((j + 1) * nf - 1))); do
                    if [ $(($l % 50)) -eq 0 ]; then
                        echo "Processing file log hadd1 "$l"/"$((nf * j + nf))
                    fi
                    cat outhadd1.$l >>$master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/outhadd1.all
                    cat errhadd1.$l >>$master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/errhadd1.all
                    cat loghadd1.$l >>$master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/loghadd1.all
                done
                cat $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/outhadd1.all >>$master/output_files/local/$1/$1_$2_$3/config$j/outhadd1.all
                cat $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/errhadd1.all >>$master/output_files/local/$1/$1_$2_$3/config$j/errhadd1.all
                cat $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/loghadd1.all >>$master/output_files/local/$1/$1_$2_$3/config$j/loghadd1.all
            fi
            if [ "${10}" == "2" ]; then
                nf=$(ls outhadd2.* | wc -l)
                for l in $(seq 0 $((nf - 1))); do
                    if [ $(($l % 50)) -eq 0 ]; then
                        echo "Processing file log hadd2 "$l"/"$nf
                    fi

                    cat outhadd2.$l >>$master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/outhadd2.all
                    cat errhadd2.$l >>$master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/errhadd2.all
                    cat loghadd2.$l >>$master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/loghadd2.all
                done
                cat $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/outhadd2.all >>$master/output_files/local/$1/$1_$2_$3/config$j/outhadd2.all
                cat $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/errhadd2.all >>$master/output_files/local/$1/$1_$2_$3/config$j/errhadd2.all
                cat $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j/loghadd2.all >>$master/output_files/local/$1/$1_$2_$3/config$j/loghadd2.all
            fi
            cd $master/output_files/local/$1/$1_$2_$3/$1_$2_$3_$k/config$j
            cp err.all err.allclean
            sed -i '/\/s]/d' err.allclean
            cp err.allclean err.xrdcp
            sed -n -i '/[ERROR]/p' err.xrdcp
            sed -n -i '/^Error/p' err.xrdcp
            cp out.all out.allclean
            sed -n -i '/^\/pnfs\//p' out.allclean
            line=$(sed -n "${k}p" ~/getflow/txts/$3\runalloffline_runlist.txt)
            echo $(cmp out.allclean ~/getflow/txts/$line\files.txt)
        done
        echo 'Finished all run for config'$j
        cd $master/output_files/local/$1/$1_$2_$3/config$j
        cp err.all err.allclean
        sed -i '/\/s]/d' err.allclean
        cp err.allclean err.xrdcp
        sed -n -i '/[ERROR]/p' err.xrdcp
        sed -n -i '/^Error/p' err.xrdcp
        cp out.all out.allclean
        sed -n -i '/^\/pnfs\//p' out.allclean
        echo $(cmp out.allclean ~/getflow/txts/$3files.txt)
    done
    if [ "$7" != "" ] && [ "$10" == "0" ]; then
        /usatlas/u/cher97/getflow/submitrerun.sh $1 $2 $3 $7 $8 $4
    fi
}

eval "$(ssh-agent -s)"
DISPLAY=1 SSH_ASKPASS="/usatlas/u/cher97/frz" ssh-add /usatlas/u/cher97/.ssh/id_ed25519 </dev/null

alias edsrc='vim ../source/MuonPerformance/Root/MyxAODAnalysis.cxx'
alias edpyst='vim ../source/MuonPerformance/share/ATestRun_eljob.py'
alias edcxxst='vim ../source/MuonPerformance/share/ATestRun_eljob.cxx'
alias edgrid='vim ../source/MuonPerformance/share/ATestSubmit.cxx'
alias edhdr='vim ../source/MuonPerformance/MuonPerformance/MyxAODAnalysis.h'
alias rungrid='rm -rf myGridJob; lsetup panda; root -l -b -q '"'"'$ROOTCOREDIR/scripts/load_packages.C'"'"' '"'"'~/flow/source/MyAnalysis/share/ATestSubmit.cxx("myGridJob")'"'"
alias runcxx='rm -rf submitDir; root -l -b -q '"'"'$ROOTCOREDIR/scripts/load_packages.C'"'"' '"'"'../source/MuonPerformance/share/ATestRun_eljob.cxx("submitDir")'"'"
alias rerun='rebuild; runlocal'
alias DATA='cd /atlasgpfs01/usatlas/data/cher97/'
alias SCRATCH='cd /usatlas/scratch/cher97/'
alias PNFS='cd /pnfs/usatlas.bnl.gov/users/cher97/'
alias LOCAL='cd /pnfs/usatlas.bnl.gov/LOCALGROUPDISK/rucio/user/xiaoning/'
alias gitgo="git add -A; git commit -m "commit"; git push"

alias gitcleargo='git add -A; git commit -m "commit for clear" ;git rm -r --cached .;git add .;git commit -m "fixed untracked files";git push'
alias gitcpgo="cp InfoHeader.h ../atlasstyle-00-04-02/InfoHeader.h; cp InfoHeader.h ../offline_flow/InfoHeader.h; gitgo"

localSetupRucioClients
#voms-proxy-init --voms=atlas -hours 240 -old
alias SCR='cd /usatlas/scratch/cher97'
alias raccount='voms-proxy-init --voms=atlas -hours 240 --valid 240:00 --vomslife 240:00'
alias RSET='lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"'
alias RSETMV='lsetup "root 6.04.02-x86_64-slc6-gcc48-opt"'
alias root='root -l'

stty erase ''
