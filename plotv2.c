const int font = 12;
void plotv2()
{
    double ls = 0.04;
    double mu = 1;
    double v = 1.5;
    double r = 0.8;
    int pt = 2;

    TCanvas *c1 = new TCanvas("c1", "c1", 1200, 1000);
    TPad *p0 = (TPad *)c1->cd();
    p0->SetTopMargin(0.05);
    p0->SetBottomMargin(0.13);
    TH1F *h0 = (TH1F *)p0->DrawFrame(0., 0.0, 4 * mu, 0.53);
    TF1 *f = new TF1("f1", "[0]*TMath::Power(([1]/[2]),((x-[4]*[3])/[2]))*(TMath::Exp(-([1]/[2])))/TMath::Gamma(((x-[4]*[3])/[2])+1.)", 0, 4);
    f->SetParameters(1, 1, v, v, r);
    // TF1 *f = new TF1("f", "TMath::Cos(x/TMath::Pi())", -pi, pi);
    TH1 *h = f->GetHistogram();
    h->SetLineColor(TColor::GetColor("#ffffff"));
    h->SetMarkerColor(TColor::GetColor("#ffffff"));
    h->SetTitle("");
    TAxis *a = h0->GetXaxis();
    a->SetTitleFont(font);
    a->SetNdivisions(005);
    a->SetLabelSize(ls);
    a->ChangeLabel(2, -1, ls, 0, -1, -1, Form("#it{#mu}|_{p_{T}=%d GeV}", pt));
    a->ChangeLabel(3, -1, ls, 0, -1, -1, Form("2#it{#mu}|_{p_{T}=%d GeV}", pt));
    a->ChangeLabel(4, -1, ls, 0, -1, -1, Form("3#it{#mu}|_{p_{T}=%d GeV}", pt));
    a->ChangeLabel(5, -1, ls, 0, -1, -1, Form("4#it{#mu}|_{p_{T}=%d GeV}", pt));
    a->ChangeLabel(6, -1, ls, 0, -1, -1, Form("5#it{#mu}|_{p_{T}=%d GeV}", pt));
    a->SetTitle("v_{n}#it{#left{2#right}}");
    a->SetTitleSize(ls * 1.1);
    a->SetTitleOffset(1.4);

    TAxis *b = h0->GetYaxis();
    b->SetNdivisions(005);
    b->SetLabelSize(ls);
    b->SetTitleFont(font);
    b->SetTitle("Probability");
    b->SetTitleSize(ls * 1.1);
    b->SetTitleOffset(0.98);

    h0->Draw();
    // f->Draw("SAME");
    //cout << h->GetXaxis()->GetNbins() << endl;
    //cout << h->GetBinContent(10) << endl;
    //cout << h->GetBinCenter(10) << endl;
    //TFile* fo = TFile::Open("output.root","RECREATE");
    //h->Write();
    //fo->Close();
    h->Draw("SAME PL");
    a->Draw("SAME");
    gPad->Update();
    c1->SaveAs(Form("mu%.1f_v%.1f_r%.1f.png", mu, v, r));
    c1->SaveAs(Form("mu%.1f_v%.1f_r%.1f.pdf", mu, v, r));
    // TGaxis *ax = new TGaxis(0, gPad->GetUymin(),
    //                         5, gPad->GetUymin(),
    //                         0, 5, 005, "");
    // ax->SetLabelSize(0.);
    // ax->Draw();
}