#!/bin/bash
version=FlowOL240814.1
nversion=FlowOL240814.1
dataset=data18mb0aod2cb
ndataset=$dataset
nconfigs=2
input=/usatlas/u/cher97/getflow/txts/$dataset\runalloffline_runlist.txt

nsubs=10
st=1
ed=44

nds=${ndataset%"cb"}\all
dest=/usatlas/u/cher97/output_files/local/offline_flow/offline_flow_$nversion\_$nds/
origin=/usatlas/u/cher97/output_files/local/offline_flow/offline_flow_$version\_$dataset
mkdir -p $dest
for i in $(seq 0 $((nconfigs - 1))); do
    folder=$dest/config$i
    echo $folder
    mkdir -p $folder
    for j in $(seq 0 $((nsubs - 1))); do
        linenumber=1
        >$folder/sub$nsubs.$j.source.txt
        while IFS= read -r line <&3; do
            if [ $linenumber -ge $st ] && [ $linenumber -le $ed ]; then
                f=$(ls $origin/offline_flow_$version\_$dataset\_$linenumber/config$i/sub$nsubs.$j.root)
                #cho 'file: '$f
                echo $f >>$folder/sub$nsubs.$j.source.txt
                #echo $source
            fi
            linenumber=$((linenumber + 1))
        done 3<$input
        hadd -f -v 1 $folder/sub$nsubs.$j.root @$folder/sub$nsubs.$j.source.txt
        #cp $folder/sub$nsubs.$j.root $folder/sub$nsubs.$j.root
    done
    hadd -f $folder/tots.root $folder/sub$nsubs.*.root
done