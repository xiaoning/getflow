#!/bin/bash
#./runhadd_round1.sh $1 foldername:spmethod $2 tag $3 data $4:nsubs $5:do nfiles round $6: nconfigs $7:dest

home=/usatlas/u/cher97
input=$home/getflow/txts/$3runall_runlist.txt
if [ "$7" == "" ]; then
    master='/atlasgpfs01/usatlas/data/cher97'
else
    master='/pnfs/usatlas.bnl.gov/users/cher97'
fi
timestamp=$(date +%s)

st=9
ed=10

linenumber=1

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

nconfigs=$6

while IFS= read -r line <&3; do
    #echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
    folder=$1/$1\_$2\_$3/$1_$2_$3_$linenumber

    if [ "$line" = "" ] || [ "$line" = "done" ]; then
        break
    fi
    nhadd=0
    if [ $linenumber -gt $st ] && [ $linenumber -le $ed ]; then
        # create list of files in txt
        for k in $(seq 0 $((nconfigs - 1))); do
            for j in $(seq 0 $((nsubs - 1))); do
                dest=$master/$folder/config$k/sub$nsubs\.$j
                ls $dest/*_*.root >$dest/totalfiles.txt
                totn=$(wc -l <$dest/totalfiles.txt)
                nf=$5
                n=$(((totn + nf - 1) / nf))
                python /usatlas/u/cher97/getflow/splitfiles.py $dest totalfiles $5
                nhadd=$((nhadd + n))
            done
        done

        input_loc=/usatlas/u/cher97/getflow/txts/$folder/
        mkdir -p $input_loc
        ls $master/$folder/config*/sub$nsubs.*/totalfiles_*.txt >$input_loc/totalfiles_condor.txt

        for i in $(seq 0 $((nhadd - 1))); do
            ./runhadd_round1_localloop.sh $i $input_loc/totalfiles_condor.txt $timestamp
        done
    #sed -i "s@^Executable.*@Executable   = /usatlas/u/cher97/getflow/runhadd_round1loop.sh@" $home/getflow/condors/$folder'_runhadd_round1.job'
    #sed -i "s@^Arguments.*@Arguments       = \$(Process) $input_loc/totalfiles_condor.txt $timestamp@" $home/getflow/condors/$folder'_runhadd_round1.job'
    #sed -i "s@^Queue.*@Queue $nhadd@" $home/getflow/condors/$folder'_runhadd_round1.job'

    #condor_submit $home/getflow/condors/$folder'_runhadd_round1.job'
    fi
    linenumber=$((linenumber + 1))
done 3<$input
