#include <TROOT.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <dirent.h>
#include "TFile.h"
#include "TChain.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TApplication.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TSystem.h"
#include <algorithm>
#include "TStyle.h"
#include "TColor.h"
#include "TProfile.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include <math.h>

#include <TH2.h>
#include <TStyle.h>

/*#include "../atlasstyle-00-04-02/AtlasUtils.h"
#include "../atlasstyle-00-04-02/AtlasStyle.h"
#include "../atlasstyle-00-04-02/AtlasLabels.h"
#include "../atlasstyle-00-04-02/AtlasStyle.C"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "../atlasstyle-00-04-02/AtlasLabels.C"
#include "../atlasstyle-00-04-02/AtlasUtils.C"
#endif

#ifdef __CINT__
gROOT->LoadMacro("../atlasstyle-00-04-02/AtlasLabels.C");
gROOT->LoadMacro("../atlasstyle-00-04-02/AtlasUtils.C");
#endif
*/

#include "/usatlas/u/cher97/offline_flow/Flow.h"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "/usatlas/u/cher97/offline_flow/Flow_main.cpp"
#include "/usatlas/u/cher97/offline_flow/Flow.cpp"
#include "/usatlas/u/cher97/offline_flow/corr.cpp"
#include "/usatlas/u/cher97/offline_flow/corr_trk.cpp"
#include "/usatlas/u/cher97/offline_flow/corr_qn.cpp"
#include "/usatlas/u/cher97/offline_flow/inits.cpp"
#include "/usatlas/u/cher97/offline_flow/performances.cpp"
#include "/usatlas/u/cher97/offline_flow/Utils.cpp"
#include "/usatlas/u/cher97/offline_flow/setters.cpp"
#include "/usatlas/u/cher97/offline_flow/qn.cpp"
#include "/usatlas/u/cher97/offline_flow/evt_perf.cpp"
#include "/usatlas/u/cher97/offline_flow/evt_reject.cpp"
#include "/usatlas/u/cher97/offline_flow/track_methods.cpp"
#include "/usatlas/u/cher97/offline_flow/corr_nested.cpp"
#include "/usatlas/u/cher97/offline_flow/sp.cpp"
#include "/usatlas/u/cher97/offline_flow/sp_qn.cpp"
#include "/usatlas/u/cher97/offline_flow/sp_trk.cpp"
#include "/usatlas/u/cher97/offline_flow/qn_tree.cpp"
#endif

#ifdef __CINT__
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/Flow_main.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/Flow.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/corr.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/corr_trk.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/corr_qn.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/inits.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/performances.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/Utils.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/setters.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/qn.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/evt_perf.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/evt_reject.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/track_methods.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/corr_nested.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/sp.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/sp_qn.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/sp_trk.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/qn_tree.cpp");
#endif

// const std::string fin = "/usatlas/u/cher97/test/240422SPOldNewTest1EventRBRFCalRC/flowtree/data18_hi.00365678.physics_CC.merge.AOD.f1030_m2048.root";
// const std::string fin = "/usatlas/u/cher97/test/CC/data18peb0/365678/user.xiaoning.34252508._000001.myFlow.root";
// const std::string fin = "/usatlas/u/cher97/test/PC/data18peb0/367233/user.xiaoning.34227307._000011.myFlow.root";
const std::string fin = "CC80Qn.0.root";
// const std::string fin = "/usatlas/u/cher97/test/qntree/flow_0_0-2.1.0.root";
const std::string fout = "TestConfig0SPNom";
const std::string findstr = "AAA";

const std::vector<double> pT_range_config = cms_pT_range_fine2;
const std::vector<double> ctr_range_config = cms_ctr_range_new60;
const bool m_use_mult = false;
const std::vector<int> N_config = {2, 3};
const std::vector<int> Qn_N_config = {2, 3, 4, 6};
const int nsubs_config = 10;
const int datatype_id = 0;
const int m_weight_tag = 1; // 0 for uniform weight, 1 for actual weight, 2 for random
const TString m_version = "FlowOL231019.1";
const TString m_dataset = "data18mb0";
const int linenumber = 32;
const int config = 0;
const TString m_dest = "/atlasgpfs01/usatlas/data/cher97";
const TString m_wtversion = "FlowOL240303_1";
const TString m_rcidversion = "FlowOL240610_1";
const TString m_rcfcalversion = "FlowOL240624_1";
const std::vector<int> m_refeta_id = {12, 13, 14, 15};
const std::vector<int> m_poieta_id = {12, 13, 14, 15};
const double m_refptlow = 0.5;
const double m_refpthigh = 5.0;
const int m_zvtx_id = 1;
const bool m_of = true;
const bool m_perf = false;
const bool m_corr_trk = false;
const bool m_corr_qn = false;
const bool m_sp_trk = false;
const bool m_sp_qn = true;
const bool m_fcal_perf = true;
const bool m_qn = false;

const bool m_qn_tree = false;
const std::vector<int> m_subevents = {5, 6, 7};

const bool m_event = false;
const bool m_evt_perf = false;
const bool m_evt_reject = false;
const int m_tree_option = 2;
const std::string m_branch_suffix = "";

const bool m_id_rc = true;
const bool m_id_rc_allrun = true;

const bool m_fcal_rc = true;
const bool m_fcal_rc_allrun = true; // halves available in allrun mode only

const int m_eta_fcal_index = 0; // for sp
const int m_eta_id_index = 6;   // for sp

const int m_eta_qn_index = 18;

const bool m_mc = true;

const bool m_weight = true;
const bool m_weight_allrun = false;

const bool m_event_filter = false;

const bool m_prescale_correction = true;

const bool m_cov = false;
const bool m_test = false;

const int m_puop = 1111;
const int m_ootpsides = 11;
const TString m_ootpname = "240129_11sigma";
const TString m_ootpntname = "240301_11sigma";
const int m_itpsides = 1;
const TString m_itpname = "240219_31sigma";
const int m_subevent = 0;

const bool m_rem_only = false;
const int m_rem = 0;
const std::vector<bool> selections_config = {0, 1, 1, 1}; //"both hits in inner most tracker","d0 pull cut 3.0", "z0sintheta pull cut 3.0", "HITight"

void exec(std::string in, std::string out)
{
    gInterpreter->GenerateDictionary("vector<vector<double> >", "vector");
    gInterpreter->GenerateDictionary("vector<vector<vector<double> > >", "vector");
    gInterpreter->GenerateDictionary("vector<vector<vector<vector<double> > > >", "vector");
    gInterpreter->GenerateDictionary("vector<vector<int> >", "vector");
    gInterpreter->GenerateDictionary("vector<vector<double> >", "vector");

    std::cout << "Entering main" << std::endl;
    // make object
    Flow *flow = new Flow(fin, fout, datatype_id, 0, nsubs_config, findstr, m_tree_option, m_branch_suffix);
    // evt performance
    flow->setEvtPerf(m_evt_perf);
    // evt rejection
    flow->setEvtReject(m_evt_reject);
    // performance
    flow->setPerformance(m_perf);
    // Qn
    flow->setQn(m_qn);
    flow->setNsQn(Qn_N_config);
    // Fcal
    flow->setFcal(m_fcal_perf);
    flow->setSPTrk(m_sp_trk);
    flow->setSPQn(m_sp_qn);
    flow->setEtaFcalIndex(m_eta_fcal_index);
    flow->setEtaIDIndex(m_eta_id_index);
    flow->setEtaQnIndex(m_eta_qn_index);

    // Qn tree
    flow->setQnTree(m_qn_tree);
    flow->setSubevents(m_subevents);

    // correlators
    flow->setCorrTrk(m_corr_trk);
    flow->setCorrQn(m_corr_qn);
    flow->setNs_corr(N_config);
    flow->setOverflow(m_of);
    flow->setCov(m_cov);
    flow->setTest(m_test);

    // recentering
    flow->setRCID(m_id_rc, m_id_rc_allrun);
    flow->setRcIDVersion(m_rcidversion);
    flow->setRCFcal(m_fcal_rc, m_fcal_rc_allrun);
    flow->setRcFcalVersion(m_rcfcalversion);
    // weight
    flow->setWeightTag(m_weight_tag);
    flow->setWtVersion(m_wtversion);
    flow->setTrkCorrection(m_weight, m_weight_allrun);
    flow->setMCCorrection(m_mc);
    flow->setPrescaleCorrection(m_prescale_correction);
    flow->setSubevent_corr(m_subevent);
    // event filter
    flow->setEventFilter(m_event_filter);
    // kinematics
    flow->setRefPt(m_refptlow, m_refpthigh);
    flow->setRefEta(m_refeta_id);
    flow->setPoiEta(m_poieta_id);
    flow->setZVtx(m_zvtx_id);
    flow->setpTRange(pT_range_config);
    flow->setctrRange(ctr_range_config, m_use_mult);

    // event detail
    flow->setDoEvent(m_event);
    flow->setStopAtN(100);
    flow->setStopAtNDirectory(Form("%s/offline_flow/offline_flow_%s_%s_%d/config%d/csv/", m_dest.Data(), m_version.Data(), m_dataset.Data(), linenumber, config));
    flow->setOOTPVersion(m_ootpname);
    flow->setITPVersion(m_itpname);
    flow->setOOTPNTVersion(m_ootpntname);
    flow->setOOTPMSides(m_ootpsides);
    flow->setITPMSides(m_itpsides);
    flow->setPUOP(m_puop);
    flow->setRem(m_rem);
    flow->setRemOnly(m_rem_only);

    // run configs
    // flow->setNMaxEvents(10000);
    flow->setVerbose(0);

    // Run
    flow->initBranches();
    flow->initTrackSelections(selections_config);
    flow->Loop();
    // init output path
    // init histogram
    // fill histogram (loop)
    // output
    return;
}
