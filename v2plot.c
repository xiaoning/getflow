const int font = 12;
const double llx = 0.18;
const double lrx = 0.5;
const double lby = 0.11;
const double lty = 0.43;
const double ptm = 0.05;
const double pbm = 0.13;
const double plm = 0.1;
const double prm = 0.1;

const double ptm1 = 0.05;
const double pbm1 = 0.2;
const double plm1 = 0.23;
const double prm1 = 0.04;
const double yos1 = 1.2;

const double ptm2 = 0.05;
const double pbm2 = 0.2;
const double plm2 = 0.23;
const double prm2 = 0.04;
const double yos2 = 1.25;

const double rlx = 0.58;
const double rrx = 0.91;
const double rby = 0.53;
const double rty = 0.85;
const double xos = 0.89;
const double yos = 1.4;
const double clm = 0.15;
const double crm = 0.05;
const double ctm = 0.1;
const double cbm = 0.1;

const double xa = 0.24;
const double ya = 0.65;

const double xb = 0.755;
const double yb = 0.31;

#include "../atlasstyle-00-04-02/InfoHeader.h"
#include "../atlasstyle-00-04-02/InfoHeaderUtilities.h"

#include "/usatlas/u/cher97/atlasstyle-00-04-02/AtlasUtils.h"
#include "/usatlas/u/cher97/atlasstyle-00-04-02/AtlasStyle.h"
#include "/usatlas/u/cher97/atlasstyle-00-04-02/AtlasLabels.h"
#include "/usatlas/u/cher97/atlasstyle-00-04-02/AtlasStyle.C"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "/usatlas/u/cher97/atlasstyle-00-04-02/AtlasLabels.C"
#include "/usatlas/u/cher97/atlasstyle-00-04-02/AtlasUtils.C"
#endif

#ifdef __CINT__
gROOT->LoadMacro("/usatlas/u/cher97/atlasstyle-00-04-02/AtlasLabels.C");
gROOT->LoadMacro("/usatlas/u/cher97/atlasstyle-00-04-02/AtlasUtils.C");
#endif

void v2plot()
{
    TTree *t = new TTree("t", "v2plot.csv");
    t->ReadFile("v2plot.csv", "yt0_X:yt0_Y:yt0_XL:yt0_XH:yt0_YL:yt0_YH");
    TCanvas *c = new TCanvas("c", "c", 1000, 1000);
    c->cd();
    TPad *p = new TPad("p", "p", 0, 0, 1, 1, kWhite, 0, 0);
    p->SetLogx(1);
    c->SetLeftMargin(clm);
    c->SetRightMargin(crm);
    c->SetTopMargin(ctm);
    c->SetBottomMargin(cbm);
    TH1F *h = (TH1F *)p->DrawFrame(1., -0.05, 130., 0.25);
    // TLatex ytitle = new TLatex()
    h->GetYaxis()->SetTitle("v_{n}     (p_{#it{T}})");
    h->GetXaxis()->SetTitle("p_{#it{T}}#it{[GeV]}");
    h->GetYaxis()->SetTitleOffset(1.6);
    h->GetXaxis()->SetTitleOffset(1.2);
    h->GetXaxis()->SetRangeUser(1., 130.);
    Float_t x, y, exl, exh, eyl, eyh;
    t->SetBranchAddress("yt0_X", &x);
    t->SetBranchAddress("yt0_Y", &y);
    t->SetBranchAddress("yt0_XL", &exl);
    t->SetBranchAddress("yt0_XH", &exh);
    t->SetBranchAddress("yt0_YL", &eyl);
    t->SetBranchAddress("yt0_YH", &eyh);
    TGraphAsymmErrors *h0 = new TGraphAsymmErrors(22);
    for (int i = 1; i <= 22; i++)
    {
        t->GetEntry(i - 1);
        h0->SetPoint(i, x, y);
        h0->SetPointError(i, exl, exh, eyl, eyh);
    }
    h0->SetMarkerStyle(20);
    h0->SetMarkerSize(2);
    h0->SetLineWidth(2);
    h0->SetMarkerColor(kBlue);
    h0->SetLineColor(kBlue);
    h->SetLineColor(kWhite);
    // h0->SetFillColor(kBlue);
    // h0->SetFillColorAlpha(kBlue, 0.3);
    // h0->SetFillStyle(3001);

    // gStyle->SetTitleFont(12,"x");
    // gStyle->SetTitleFont(12,"y");
    h->GetYaxis()->SetTitleFont(font);
    h->GetXaxis()->SetTitleFont(font);
    h->Draw();
    h0->GetXaxis()->SetRangeUser(1., 130.);
    h0->GetYaxis()->SetRangeUser(-0.05, 0.25);
    h0->Draw("SAME PE");
    c->SaveAs("v2plot.pdf");
    c->SaveAs("v2plot.png");
    c->SaveAs("v2plot.jpeg");

    double ls = 0.09;
    double mu = 1;
    double v = 0.3;
    double r = 0.6;
    int pt = 2;
    c->cd();
    TPad *p1 = new TPad("p1", "", llx, lby, lrx, lty, kWhite, 0, -1);
    // p1->SetLogx(0);
    p1->Draw();
    p1->cd();
    p1->SetTopMargin(ptm1);
    p1->SetBottomMargin(pbm1);
    p1->SetLeftMargin(plm1);
    p1->SetRightMargin(prm1);
    TH1F *h1 = (TH1F *)p1->DrawFrame(0., 0.0, 4 * mu, 0.4);
    TF1 *f1 = new TF1("f1", "[0]*TMath::Power(([1]/[2]),((x-[4]*[3])/[2]))*(TMath::Exp(-([1]/[2])))/TMath::Gamma(((x-[4]*[3])/[2])+1.)", 0, 4);
    // a*e*(1-e^2)^(a-1)
    f1->SetParameters(10, 1, v, v, r);
    // TF1 *f = new TF1("f", "TMath::Cos(x/TMath::Pi())", -pi, pi);
    TH1 *hf1 = f1->GetHistogram();
    hf1->Scale(10. / hf1->GetSumOfWeights());
    hf1->SetLineColor(TColor::GetColor("#ffffff"));
    hf1->SetMarkerColor(TColor::GetColor("#ffffff"));
    hf1->SetTitle("");
    TAxis *a1 = h1->GetXaxis();
    a1->SetTitleFont(12);
    a1->SetNdivisions(003, false);
    a1->SetLabelSize(ls);
    a1->ChangeLabel(2, -1, ls, 0, -1, -1, "#it{#mu}");
    a1->ChangeLabel(3, -1, ls, 0, -1, -1, "2#it{#mu}");
    a1->ChangeLabel(4, -1, ls, 0, -1, -1, "3#it{#mu}");

    // a1->ChangeLabel(5, -1, ls, 0, -1, -1, Form("4#it{#mu}|_{p_{T}=%d GeV}", pt));
    // a1->ChangeLabel(6, -1, ls, 0, -1, -1, Form("5#it{#mu}|_{p_{T}=%d GeV}", pt));
    a1->SetTitle("v_{n}   ");
    a1->SetTitleSize(ls * 1.1);
    a1->SetTitleOffset(xos);

    TAxis *b1 = h1->GetYaxis();
    b1->SetNdivisions(005);
    b1->SetLabelSize(ls);
    b1->SetTitleFont(font);
    b1->SetTitle("Probability");
    b1->SetTitleSize(ls * 1.1);
    b1->SetTitleOffset(yos1);
    // b1->SetMaxDigits(2);

    h1->Draw();
    // cout << hf1->GetBinContent(10) << endl;
    hf1->SetLineColor(kRed);
    hf1->SetLineWidth(2);
    hf1->SetLineStyle(1);
    hf1->SetMarkerColor(kRed);
    hf1->SetMarkerStyle(1);
    hf1->SetMarkerSize(1);
    // a1->SetMaxDigits(2);
    hf1->Draw("SAME HIST PL");
    a1->Draw("SAME");
    TLatex l1; //
    l1.SetTextAlign(font);
    l1.SetTextSize(ls * 0.9);
    l1.SetTextFont(42);
    l1.SetNDC();
    l1.SetTextColor(kBlack);
    l1.DrawLatex(1 - ls - prm1 - 0.01, pbm1 * 0.4 - 0.01, "{2}");
    c->SetLogx(1);
    c->Update();
    // c->SaveAs(Form("v2plot_mu%.1f_v%.1f_r%.1f.png", mu, v, r));
    c->SaveAs(Form("v2plot_mu%.1f_v%.1f_r%.1f.pdf", mu, v, r));

    ls = 0.09;
    mu = 1;
    v = 1.5;
    r = 0.8;
    pt = 40;

    c->cd();
    TPad *p2 = new TPad("p2", "", rlx, rby, rrx, rty, kWhite, 1, -1);
    // p1->SetLogx(0);
    p2->Draw();
    p2->cd();
    p2->SetTopMargin(ptm2);
    p2->SetBottomMargin(pbm2);
    p2->SetLeftMargin(plm2);
    p2->SetRightMargin(prm2);
    TH1F *h2 = (TH1F *)p2->DrawFrame(0., 0.03, 4 * mu, 0.18);
    TF1 *f2 = new TF1("f2", "[0]*TMath::Power(([1]/[2]),((x-[4]*[3])/[2]))*(TMath::Exp(-([1]/[2])))/TMath::Gamma(((x-[4]*[3])/[2])+1.)", 0, 4);
    f2->SetParameters(10, 1, v, v, r);
    // TF1 *f = new TF1("f", "TMath::Cos(x/TMath::Pi())", -pi, pi);
    TH1 *hf2 = f2->GetHistogram();
    hf2->SetLineColor(TColor::GetColor("#ffffff"));
    hf2->SetMarkerColor(TColor::GetColor("#ffffff"));
    hf2->SetTitle("");
    hf2->Scale(10. / hf2->GetSumOfWeights());
    hf2->SetLineColor(kRed);
    hf2->SetLineWidth(2);
    hf2->SetLineStyle(1);
    hf2->SetMarkerColor(kRed);
    hf2->SetMarkerStyle(1);
    hf2->SetMarkerSize(1);
    // a1->SetMaxDigits(2);

    TAxis *a2 = h2->GetXaxis();
    a2->SetTitleFont(font);
    a2->SetNdivisions(002, false);
    a2->SetLabelSize(ls);

    a2->ChangeLabel(2, -1, ls, 0, -1, -1, "#it{#mu}");
    a2->ChangeLabel(3, -1, ls, 0, -1, -1, "2#it{#mu}");
    a2->ChangeLabel(4, -1, ls, 0, -1, -1, "3#it{#mu}");
    // a2->ChangeLabel(5, -1, ls, 0, -1, -1, Form("4#it{#mu}|_{p_{T}=%d GeV}", pt));
    // a2->ChangeLabel(6, -1, ls, 0, -1, -1, Form("5#it{#mu}|_{p_{T}=%d GeV}", pt));
    a2->SetTitle("v_{n}   ");
    a2->SetTitleSize(ls * 1.1);
    a2->SetTitleOffset(xos);

    TAxis *b2 = h2->GetYaxis();
    b2->SetNdivisions(005);
    b2->SetLabelSize(ls);
    b2->SetTitleFont(font);
    b2->SetTitle("Probability");
    b2->SetTitleSize(ls * 1.1);
    b2->SetTitleOffset(yos2);

    h2->Draw();
    // f2->Draw("SAME");
    hf2->Draw("SAME HIST L");
    a2->Draw("SAME");
    TLatex l2; //
    l2.SetTextAlign(12);
    l2.SetTextSize(ls * 0.9);
    l2.SetTextFont(42);
    l2.SetNDC();
    l2.SetTextColor(kBlack);
    l2.DrawLatex(1 - ls - prm2, pbm2 * 0.4 - 0.01, "{2}");

    c->cd();
    TPad *p3 = new TPad("all", "all", 0, 0, 1, 1);
    p3->SetFillStyle(4000);
    p3->Draw();
    p3->cd();
    TLine la;
    la.SetLineStyle(2);
    la.SetLineColor(kBlue);
    la.DrawLineNDC(llx + (lrx - llx) * plm1, lty - (lty - lby) * ptm1, xa, ya);
    TLine lb;
    lb.SetLineStyle(2);
    lb.SetLineColor(kBlue);
    lb.DrawLineNDC(llx + (lrx - llx) * (1 - prm1), lty - (lty - lby) * ptm1, xa, ya);

    TLine lc;
    lc.SetLineStyle(2);
    lc.SetLineColor(kBlue);
    lc.DrawLineNDC(rlx + (rrx - rlx) * plm2, rby + (rty - rby) * pbm2, xb, yb);
    TLine ld;
    ld.SetLineStyle(2);
    ld.SetLineColor(kBlue);
    double ox = rlx + (rrx - rlx) * (1 - prm2);
    double oy = rby + (rty - rby) * pbm2;
    double ratio = 0.8;
    ld.DrawLineNDC(ox * ratio + xb * (1 - ratio), oy * ratio + yb * (1 - ratio), xb, yb);

    TLatex l0;
    l0.SetTextAlign(12);
    l0.SetTextSize(0.03);
    l0.SetTextAngle(90);
    l0.SetTextFont(42);
    l0.SetNDC();
    l0.SetTextColor(kBlack);
    l0.DrawLatex(0.06, 0.81, "{2}");
    c->SetLogx(1);
    c->Update();

    myText(0.32, 0.38, kBlack, Form("#it{p}_{T} = %d GeV", 2), 0.03);
    myText(0.72, 0.8, kBlack, Form("#it{p}_{T} = %d GeV", 40), 0.03);
    // c->SaveAs(Form("v2plot_mu%.1f_v%.1f_r%.1f.png", mu, v, r));
    c->SaveAs(Form("v2plot2_mu%.1f_v%.1f_r%.1f.pdf", mu, v, r));
}
