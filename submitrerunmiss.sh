#!/bin/bash
#./submitrerun.sh no1:location no2:MCE211203.1 no3:mc18jet no4: optional, subsampling no5:optional combine m files no6:optional n configs no7: optional partial no8:optional use pnfs
#copy and open to be edited steering macro
home=/usatlas/u/cher97
master=/atlasgpfs01/usatlas/data/cher97/output_files/local/
dest=$master/$1/$1\_$2\_$3

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    comb=1
else
    comb=$5
fi

if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi

if [ "$7" == "" ]; then
    partial=1
else
    partial=$7
fi

dataset=$3

j=0
cd $dest/config$j
>err.test
sed -n "/exec\.cpp\:53\:/p" err.allclean > err.test
sed -i "s/In file included from.*$3\_//" err.test
sed -i "s/_0\/exec.cpp:53://" err.test

tmp=$(python3 $home/getflow/submitrerundisk.py $1 $2 $3 $4 $5 $j test)
a=${tmp%'['*}
a=${a%']'*}
a=${a#*'['}
#echo $a
b=${tmp##*'['}
b=${b%']'*}
#echo $b
sed -i "s/^runlist=(.*/runlist=($a)/" $home/getflow/runallofflineselect.sh
sed -i "s/^list=(.*/list=($b)/" $home/getflow/runallofflineselect.sh
vim $home/getflow/runallofflineselect.sh
cd $home/getflow