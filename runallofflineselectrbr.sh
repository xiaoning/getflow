#!/bin/bash
runlist=(2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2)
list=(752  921  985  1077  1328  1335  1336  1377  1378  1385  1394  1396  1405  1461  1465  1477  1487  1488  1515  1516  1541  1550  1561  1563  1589  1590  1593  1594  1636  1639  1648  1655  1656  1663  1670  1675  1680  1681  1682  1683  1684  1698  1699  1700  1716  1718  1723  1725  1727  1728  1729  1730  1733  1753  1754  1755  1757  1758  1759  1762  1767  1768  1781  1782  1785  1791  1799  1803  1818  1830  1831  1833  1857  1858  1859  1860  1862  1864  1865  1869  1881  1886  1892  1896  1900  1904  1906  1907  1908  1909  1910  1911  1912  1913  1914  1918  1928  1945  1947  1949  1957  1962  1963  1965  1966  1967  1971  1972  1974  1996  1997  2000  2007  2021  2023  2024  2025  2028  2029  2033  2043  2050  2055  2061  2062  2064  2070  2072  2078  2082  2085  2094  2107  2108  2109  2110  2111  2112  2113  2124  2125  2127  2128  2130  2132  2133  2134  2135  2136  2138  2143  2147  2149  2150  2152  2155  2156  2157  2158  2160  2161  2162  2163  2164  2165  2167  2171  2175  2177  2178  2179  2180  2181  2182  2183  2184  2185  2186  2187  2190  2191  2193  2195  2197  2198  2199  2206  2207  2208  2209  2210  2211  2212  2213  2214  2215  2216  2218  2220  2223  2224  2227  2229  2231  2232  2233  2235  2238  2240  2241  2242  2244  2245  2246  2247  2248  2250  2251  2252  2253  2254  2255  2256  2257  2272  2273  2274  2275  2277  2278  2279  2282  2283  2284  2285  2286  2287  2288  2289  2290  2291  2292  2293  2294  2295  2296  2298  2299  2300  2302  2303  2304  2305  2306  2307  2308  2309  2311  2313  2314  2315  2317  2319  2321  2322  2325  2326  2327  2330  2332)
nof=${#list[@]}

home=/usatlas/u/cher97

input=$home/getflow/txts/$3runalloffline_runlist.txt

raccount

if [ "$8" == "" ]; then
    master='/atlasgpfs01/usatlas/data/cher97'
else
    master='/pnfs/usatlas.bnl.gov/users/cher97'
fi
master='/usatlas/scratch/cher97'
echo $master

template=$1_$2_$3
dest=$master/$1/$template

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    comb=1
else
    comb=$5
fi

if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi

if [ "$7" == "" ]; then
    partial=1
else
    partial=$7
fi

#for ((j = 0; j < $configs; j++)); do
#    cp $home/$1/exec.cpp ~/getflow/cpp/$1/$template'_'$j'exec.cpp'
#    sed -i "s@^const int nsubs_config.*@const int nsubs_config = $nsubs;@" $home/getflow/cpp/$1/$template'_'$j'exec.cpp'
#    vim $home/getflow/cpp/$1/$template'_'$j'exec.cpp'
#done

for i in $(seq 0 $((nof - 1))); do
    linenumber=1
    line=''
    runnum=${runlist[$i]}
    ind=${list[$i]}
    while IFS= read -r linea <&3; do
        if [ $linenumber -eq $runnum ]; then
            folder=$1/$template/$template\_$linenumber
            line=$linea
            echo 'linea_'${bold}$linenumber${normal}'-------------------------------------------------------------'
            break
        fi
        if [ "$linea" = "" ] || [ "$linea" = "*done*" ]; then
            break
        fi
        linenumber=$((linenumber + 1))
    done 3<$input

    jobfile=$home/getflow/condors/$folder'_runallofflineselect_'$f'.job'
    cp $home/getflow/condors/run_temp.job $jobfile
    sed -i "s@^Output.*@Output       = $master/$folder/logFiles/out.$ind@" $jobfile
    sed -i "s@^Error.*@Error       = $master/$folder/logFiles/err.$ind@" $jobfile
    sed -i "s@^Log.*@Log       = $master/$folder/logFiles/log.$ind@" $jobfile
    sed -i "s@^Executable.*@Executable   = /usatlas/u/cher97/getflow/runallofflineloop.sh@" $jobfile
    sed -i "s@^Arguments.*@Arguments       = $ind $folder $line $nsubs $comb $configs $master $home@" $jobfile
    sed -i "s@^Queue.*@Queue 1@" $jobfile

    #exec 0<&1

    #read -p "Do you wish to remove old folder and rerun? (y/n) " answer
    #if [[ $answer =~ ^[Yy]$ ]]; then
    #echo "deleting original run folder"
    #rm -rf $dest/$folder/
    #else
    #    echo "writing to old folder"
    #fi
    #cat ~/getflow/condors/$folder'_runall.job'
    condor_submit $jobfile
done
