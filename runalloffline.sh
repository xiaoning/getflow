#!/bin/bash
if [ "$9" == "" ]; then
    user=cher97
else
    user=$9
fi
home=/usatlas/u/$user
if [ "$user" == "cher97" ]; then
    cd $home/$1
    git add -A
    git commit -m "commit for running$2"
    git tag -d $2
    git push --delete origin $2
    git tag -a $2 -m "$2"
    git push --tags
fi

if [ "${10}" == "" ]; then
    st=0
else
    st=${10}
fi

if [ "${11}" == "" ]; then
    ed=1
else
    ed=${11}
fi

input=$home/getflow/txts/$3runalloffline_runlist.txt
#runfolder='/atlasgpfs01/usatlas/data/$user'

raccount

if [ "${12}" == "" ]; then
    user2=$user
else
    user2=${12}
fi

if [ "$8" == "pnfs" ]; then
    outputdest='/pnfs/usatlas.bnl.gov/users/'$user2
else
    outputdest='/atlasgpfs01/usatlas/data/'$user2
    outputdest='/usatlas/scratch/'$user2
fi

master='/usatlas/scratch/'$user
echo $master

#./runalloffline.sh no1:location no2:MCE211203.1 no3:mc18jet no4: optional, subsampling no5:optional combine m files no6:optional n configs no7: optional partial no8:optional use pnfs
#copy and open to be edited steering macro

template=$1_$2_$3
dest=$master/$1/$template
linenumber=1

echo 'start: '$st
echo 'end: '$ed

mkdir -p $home/getflow/cpp/$1/$template
mkdir -p $home/getflow/condors/$1/$template

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    comb=1
else
    comb=$5
fi

if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi

if [ "$7" == "" ]; then
    partial=1
else
    partial=$7
fi

#for ((j = 0; j < $configs; j++)); do
#    cp $home/$1/exec.cpp $home/getflow/cpp/$1/$template'_'$j'exec.cpp'
#    sed -i "s@^const int nsubs_config.*@const int nsubs_config = $nsubs;@" $home/getflow/cpp/$1/$template'_'$j'exec.cpp'
#    vim $home/getflow/cpp/$1/$template'_'$j'exec.cpp'
#done

#save source to each run folder
#mkdir -p $outputdest/$1
#chmod +777 -R $outputdest/$1
#mkdir -p $dest/$1
#chmod +777 -R $dest/$1
for ((j = 0; j < $configs; j++)); do
    echo 'config '$j
    mkdir -p $dest/config$j/cpp
    rsync -a $home/$1 $dest/config$j/cpp/ --exclude .git
    mv $dest/config$j/cpp/$1 $dest/config$j/cpp/temp_folder
    cp -r $dest/config$j/cpp/temp_folder $dest/config$j/cpp/$1
    rm -rf $dest/config$j/cpp/temp_folder
    #sed -i "s@^const int nsubs_config.*@const int nsubs_config = $nsubs;@" $dest/config$j/cpp/$1/exec.cpp
    #if [ "$user" == "cher97" ]; then
    #    echo "now editing the exectutable"
    #    vim $dest/config$j/cpp/$1/exec.cpp
    #else
    cp $home/$1/exec_config$j.cpp $dest/config$j/cpp/$1/exec.cpp
    echo "changing all cpp files"
    for cfile in $dest/config$j/cpp/$1/*.cpp; do
        sed -i "s@$home/$1@$dest/config$j/cpp/$1@g" $cfile
    done
    echo "changing all header files"
    for cfile in $dest/config$j/cpp/$1/*.h; do
        sed -i "s@$home/$1@$dest/config$j/cpp/$1@g" $cfile
    done
    sed -i "s@^const int nsubs_config.*@const int nsubs_config = $nsubs;@" $dest/config$j/cpp/$1/exec.cpp

    #fi
    cp $dest/config$j/cpp/$1/exec.cpp $home/getflow/cpp/$1/$template/$template'_'$j'exec.cpp'
done

while IFS= read -r line <&3; do
    echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
    folder=$1/$template/$template\_$linenumber
    if [ "$line" = "" ] || [ "$line" = "*done*" ]; then
        break
    fi
    if [ $linenumber -gt $st ] && [ $linenumber -le $ed ]; then
        #JZ=-1
        #if [[ "$3" = *"mc"*"jet"* ]]; then
        #    tmps=${line#*JZ}
        #    JZ=${tmps:0:1}
        #    sed -i "s@.*alg.JZ =.*@alg.JZ = $JZ@" $home/getflow/py/$folder'ATestRun_eljob.py'
        #fi

        if [ "$line" = "" ]; then
            break
        fi

        cp $home/getflow/condors/run_temp.job $home/getflow/condors/$folder'_runalloffline.job'

        sed -i "s@^Output.*@Output       = $master/$folder/logFiles/out.\$(Process)@" $home/getflow/condors/$folder'_runalloffline.job'
        sed -i "s@^Error.*@Error       = $master/$folder/logFiles/err.\$(Process)@" $home/getflow/condors/$folder'_runalloffline.job'
        sed -i "s@^Log.*@Log       = $master/$folder/logFiles/log.\$(Process)@" $home/getflow/condors/$folder'_runalloffline.job'
        sed -i "s@^Executable.*@Executable   = $home/getflow/runallofflineloop.sh@" $home/getflow/condors/$folder'_runalloffline.job'
        sed -i "s@^Arguments.*@Arguments       = \$(Process) $folder $line $nsubs $comb $configs $outputdest $home@" $home/getflow/condors/$folder'_runalloffline.job'
        nofful=$(wc -l <$home/getflow/txts/$line.txt)
        nof=$(((nofful + comb - 1) / comb))
        nof=$((nof / $partial))
        sed -i "s@^Queue.*@Queue $nof@" $home/getflow/condors/$folder'_runalloffline.job'

        #exec 0<&1

        #read -p "Do you wish to remove old folder and rerun? (y/n) " answer
        #if [[ $answer =~ ^[Yy]$ ]]; then
        echo "deleting original run folder"
        rm -rf $master/$folder/
        echo "deleting original output folder"
        rm -rf $outputdest/$folder
        mkdir -p $outputdest/$folder
        #else
        #    echo "writing to old folder"
        #fi

        mkdir -p $master/$folder/
        mkdir -p $master/$folder/logFiles/

        for ((j = 0; j < $configs; j++)); do
            mkdir -p $outputdest/$folder/config$j/csv
            mkdir -p $outputdest/$folder/config$j/cpp
            cp $home/getflow/cpp/$1/$template/$template'_'$j'exec.cpp' $home/getflow/cpp/$folder'_'$j'exec.cpp'
            sed -i "s@^.const TString m_version.*@const TString m_version = \"$2\";@" $home/getflow/cpp/$folder'_'$j'exec.cpp'
            sed -i "s@^.const int linenumber.*@const int linenumber = $linenumber;@" $home/getflow/cpp/$folder'_'$j'exec.cpp'
            for ((i = 0; i < $nsubs; i++)); do
                mkdir -p $outputdest/$folder/config$j/sub$nsubs.$i/
            done
        done
        chmod +777 -R $outputdest/$folder/
        #cat $home/getflow/condors/$folder'_runall.job'
        #cat $home/getflow/condors/$folder'_runalloffline.job'
        condor_submit $home/getflow/condors/$folder'_runalloffline.job'
    fi
    linenumber=$((linenumber + 1))
done 3<$input
