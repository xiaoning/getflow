#!/bin/bash
#./runhadd_round2.sh $1 foldername:spmethod $2 tag $3 data $4:nsubs $5:nconfigs
input=~/getflow/txts/$3runall_runlist.txt

linenumber=1

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    nconfigs=1
else
    nconfigs=$5
fi
#master=/atlasgpfs01/usatlas/data/cher97/output_files/local/
master=/usatlas/u/cher97/output_files/local
#echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
for k in $(seq 0 $((nconfigs - 1))); do
    for j in $(seq 0 $((nsubs - 1))); do
        folder=$master/$1/$1_$2_$3/$1_$2_$3_*/config$k
        dest=$master/$1/$1_$2_$3/config$k
        mkdir -p $dest
        hadd -f $dest/sub$4.$j.root $folder/sub$4.$j.root
    done
    hadd -f $dest/tots.root $dest/sub$4.*.root
done
