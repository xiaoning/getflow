import pandas as pd
import re
import os


df = pd.read_csv("txts/data18jet_run_tasklist.csv")
df_files = pd.read_csv(
    "txts/data18_hi.00100000.streamtest.merge.AOD.ktest.txt", header=None, names=['file'])
f = open("data18jet_runlist.txt", 'w')
df_files["Tasks"] = df_files['file'].apply(lambda x: int(x.split('.')[3]))
df_merge = pd.merge(df, df_files, how='left', on="Tasks")
df_files_group = df_merge.groupby("Dataset Name")
print(df_files_group["Tasks"].unique().count())
for name, group in df_files_group:
    f.write(f'{re.split(":", name)[1]}\n')
    group["file"].to_csv(
        f'txts/{re.split(":", name)[1]}.txt', header=None, index=None, sep=' ', mode='a')
