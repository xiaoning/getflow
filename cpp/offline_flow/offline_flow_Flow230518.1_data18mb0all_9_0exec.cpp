#include <TROOT.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <dirent.h>
#include "TFile.h"
#include "TChain.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TApplication.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TSystem.h"
#include <algorithm>
#include "TStyle.h"
#include "TColor.h"
#include "TProfile.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include <math.h>

#include <TH2.h>
#include <TStyle.h>

/*#include "../atlasstyle-00-04-02/AtlasUtils.h"
#include "../atlasstyle-00-04-02/AtlasStyle.h"
#include "../atlasstyle-00-04-02/AtlasLabels.h"
#include "../atlasstyle-00-04-02/AtlasStyle.C"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "../atlasstyle-00-04-02/AtlasLabels.C"
#include "../atlasstyle-00-04-02/AtlasUtils.C"
#endif

#ifdef __CINT__
gROOT->LoadMacro("../atlasstyle-00-04-02/AtlasLabels.C");
gROOT->LoadMacro("../atlasstyle-00-04-02/AtlasUtils.C");
#endif
*/

#include "/usatlas/u/cher97/offline_flow/Flow.h"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "/usatlas/u/cher97/offline_flow/Flow_main.cpp"
#include "/usatlas/u/cher97/offline_flow/Flow.cpp"
#include "/usatlas/u/cher97/offline_flow/corr.cpp"
#include "/usatlas/u/cher97/offline_flow/inits.cpp"
#include "/usatlas/u/cher97/offline_flow/performances.cpp"
#include "/usatlas/u/cher97/offline_flow/Utils.cpp"
#endif

#ifdef __CINT__
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/Flow_main.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/Flow.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/corr.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/inits.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/performances.cpp");
gROOT->LoadMacro("/usatlas/u/cher97/offline_flow/Utils.cpp");
#endif

const std::vector<float> pT_range_config = cms_pT_range_fine;
const std::vector<float> ctr_range_config = cms_ctr_range;
const int N_config = 2;
const int nsubs_config = 20;
const std::vector<bool> selections_config = {0, 1, 1, 1}; //"both hits in inner most tracker","HITight","d0 pull cut 3.0", "z0sintheta pull cut 3.0"


void exec(std::string in, std::string out)
{
    gInterpreter->GenerateDictionary("vector<vector<float> >", "vector");
    gInterpreter->GenerateDictionary("vector<vector<int> >", "vector");
    gInterpreter->GenerateDictionary("vector<vector<double> >", "vector");

    std::cout << "Entering main" << std::endl;
    // make object
    Flow *flow = new Flow(in, out, 0, N_config, nsubs_config);
    flow->setpTRange(pT_range_config);
    flow->setctrRange(ctr_range_config);
    flow->initBranches();
    flow->initTrackSelections(selections_config);
    flow->Loop();
    // init output path
    // init histogram
    // fill histogram (loop)
    // output
    return;
}
