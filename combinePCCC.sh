#!/bin/bash
cd /usatlas/u/cher97/getflow/txts/
input=/usatlas/u/cher97/getflow/txts/data18mb0aod2runall_runlist.txt
linenumber=0
while IFS= read -r line; do
    a=${line#*C.}
    b=${line%%physics_*}
    cat $b'physics_CC.'$a.txt $b'physics_PC.'$a.txt >$b'physics_MB.'$a.txt
    linenumber=$((linenumber + 1))
done <$input
