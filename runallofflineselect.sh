#!/bin/bash
runlist=()
list=()
nof=${#list[@]}

home=/usatlas/u/cher97

input=$home/getflow/txts/$3runalloffline_runlist.txt

raccount

if [ "$8" == "" ]; then
    master='/atlasgpfs01/usatlas/data/cher97'
else
    master='/pnfs/usatlas.bnl.gov/users/cher97'
fi
master='/usatlas/scratch/cher97'
echo $master

template=$1_$2_$3
dest=$master/$1/$template

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    comb=1
else
    comb=$5
fi

if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi

if [ "$7" == "" ]; then
    partial=1
else
    partial=$7
fi

#for ((j = 0; j < $configs; j++)); do
#    cp $home/$1/exec.cpp ~/getflow/cpp/$1/$template'_'$j'exec.cpp'
#    sed -i "s@^const int nsubs_config.*@const int nsubs_config = $nsubs;@" $home/getflow/cpp/$1/$template'_'$j'exec.cpp'
#    vim $home/getflow/cpp/$1/$template'_'$j'exec.cpp'
#done

for i in $(seq 0 $((nof - 1))); do
    linenumber=1
    line=''
    runnum=${runlist[$i]}
    ind=${list[$i]}
    while IFS= read -r linea <&3; do
        if [ $linenumber -eq $runnum ]; then
            folder=$1/$template/$template\_$linenumber
            line=$linea
            echo 'linea_'${bold}$linenumber${normal}'-------------------------------------------------------------'
            break
        fi
        if [ "$linea" = "" ] || [ "$linea" = "*done*" ]; then
            break
        fi
        linenumber=$((linenumber + 1))
    done 3<$input

    jobfile=$home/getflow/condors/$folder'_runallofflineselect_'$f'.job'
    cp $home/getflow/condors/run_temp.job $jobfile
    sed -i "s@^Output.*@Output       = $master/$folder/logFiles/out.$ind@" $jobfile
    sed -i "s@^Error.*@Error       = $master/$folder/logFiles/err.$ind@" $jobfile
    sed -i "s@^Log.*@Log       = $master/$folder/logFiles/log.$ind@" $jobfile
    sed -i "s@^Executable.*@Executable   = /usatlas/u/cher97/getflow/runallofflineloop.sh@" $jobfile
    sed -i "s@^Arguments.*@Arguments       = $ind $folder $line $nsubs $comb $configs $master $home@" $jobfile
    sed -i "s@^Queue.*@Queue 1@" $jobfile

    #exec 0<&1

    #read -p "Do you wish to remove old folder and rerun? (y/n) " answer
    #if [[ $answer =~ ^[Yy]$ ]]; then
    #echo "deleting original run folder"
    #rm -rf $dest/$folder/
    #else
    #    echo "writing to old folder"
    #fi
    #cat ~/getflow/condors/$folder'_runall.job'
    condor_submit $jobfile
done
