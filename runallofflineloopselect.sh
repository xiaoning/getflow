#!/bin/bash
#input="/atlasgpfs01/usatlas/data/cher97/mc16_5TeV.txt"
# ./runallloop.sh no1 process no2 foldername no3 inputtxt no4 nsubs no5 comb no6 configs no7 dest
# for large dataset, use pnfs for storage
list="1 4 7 10 12 13 14 15 16 17 19 23 26 28"
[[ " $list " =~ " $1 " ]] && echo 'yes' || exit

input=$8/getflow/txts/$3.txt
#input="mc16_5TeV_short.txt"
dest=$7
mkdir -p $dest/$2

#indexline=$1
linenumber=0
comb=$5
process=$1
nsubs=$4
configs=$6
bg=$((comb * process))
ed=$((comb * (process + 1)))

tempdir=$(mktemp -d)
#tempdir=/tmp/cher97/tmp.i27rom74fT/
cd $tempdir
echo $tempdir
mkdir -p $tempdir/'tempin'$2_$bg

while IFS= read -r line; do
    if [ $bg -le $linenumber -a $ed -gt $linenumber ]; then
        echo $line
        infolder=$tempdir/'tempin'$2_$bg'_'$linenumber
        mkdir -p $infolder
        xrdcp 'root://dcgftp.usatlas.bnl.gov:1096/'$line $infolder'/in.root'
        #xrdcp $line $tempdir/'tempin'$2_$bg
    fi
    #echo 'ed'$ed
    #echo 'ln'$linenumber
    if [ $ed -le $linenumber ]; then
        #echo 'break'
        break
    fi
    linenumber=$((linenumber + 1))
done <$input

# filename_noroot=${filename%.root*}

# sed -i "s@^ROOT.SH.ScanDir().filePattern(.*@ROOT.SH.ScanDir().filePattern( '*root*').scan( sh, inputFilePath )@" $tempdir/'tempout'$2_$bg/ATestRun_eljob.py

#cp $tempdir/'tempout'$2_$bg/ATestRun_eljob.py $tempdir/'tempout'$2_$bg/ATestRun_eljob$4.$i.py

for ((j = 0; j < $configs; j++)); do
    outfolder=$tempdir'/tempout'$2'_'$bg'_'$j
    mkdir -p $outfolder
    cd $outfolder
    cp $8/getflow/cpp/$2'_'$j'exec.cpp' $outfolder'/exec.cpp'
    mkdir -p $dest/$2/config$j/csv

    for ((l = $bg; l < $linenumber; l++)); do
        infolder=$tempdir/'tempin'$2'_'$bg'_'$l
        output=$outfolder'/output_'$l
        cd $infolder
        #hadd tots.root *.root
        #filename=$(ls *.root*)
        filename=$infolder'/in.root'
        echo $filename
        #filename=$tempdir/'tempin'$2_$bg/tots.root
        #echo $filename
        cd $outfolder
        root -q -b 'exec.cpp("'$filename'", "'$output'")'
    done

    cat $outfolder/*output_*.csv >>$dest/$2/config$j/csv/flow_$1_$bg-$linenumber.$4.$i'.csv'
    mkdir -p $dest/$2/config$j/cpp
    cp $outfolder'/exec.cpp' $dest/$2/config$j/cpp/flow_$1_$bg-$linenumber.$4.$i'.cpp'
    for ((i = 0; i < $nsubs; i++)); do
        mkdir -p $dest/$2/config$j/sub$4.$i/
        #ls $outfolder/*.root
        #ls $outfolder'/*_'$i'.root'
        cd $outfolder
        hadd -v 1 output_$i.root output_*.$i.root
        cp output_$i.root $dest/$2/config$j/sub$4.$i/flow_$1_$bg-$linenumber.$4.$i'.root'
    done
    sleep 2
    #rm -rf $tempdir/tempin$2_$bg_$j
    #rm -rf $tempdir/tempout$2_$bg_$j
    #rm -rf $tempdir
done

sleep 2
#rm -rf $tempdir/tempin$2_$bg
#rm -rf $tempdir/tempout$2_$bg
#rm -rf $tempdir
