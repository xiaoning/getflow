#!/bin/bash
#./submitrerun.sh no1:location no2:MCE211203.1 no3:mc18jet no4: optional, subsampling no5:optional combine m files no6:optional n configs no7: optional partial no8:optional use pnfs
#copy and open to be edited steering macro
home=/usatlas/u/cher97
master=/atlasgpfs01/usatlas/data/cher97/output_files/local/
dest=$master/$1/$1\_$2\_$3

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    comb=1
else
    comb=$5
fi

if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi

if [ "$7" == "" ]; then
    partial=1
else
    partial=$7
fi

dataset=$3

desterr=$dest/err.tmp
>$desterr
for ((j = 0; j < $configs; j++)); do
    cd $dest/config$j
    sed -n "/in.root is truncated/p" err.xrdcp >>$desterr
    sed -n "/in.root does not exist/p" err.xrdcp >>$desterr
done
sed -i "s/Error in <TFile::Init>: file \/tmp\/.*$dataset\_//" $desterr
sed -i "s/\/in.root is truncated.*recover//" $desterr
sed -i "s/Error in <TFile::TFile>: file \/tmp\/.*$dataset\_//" $desterr
sed -i "s/\/in.root does not exist//" $desterr
sed -i "s/.*non-null arguments.//" $desterr
sed -i "/^$/d" $desterr
sed -i 's![^_]*$!!' $desterr
sed -i 's/.$//' $desterr
#cat err.tmp
tmp=$(python3 $home/getflow/submitrerun.py $1 $2 $3 $4 $5 tmp)
a=${tmp%'['*}
a=${a%']'*}
a=${a#*'['}
#echo $a
b=${tmp##*'['}
b=${b%']'*}
#echo $b
sed -i "s/^runlist=(.*/runlist=($a)/" $home/getflow/runallofflineselect.sh
sed -i "s/^list=(.*/list=($b)/" $home/getflow/runallofflineselect.sh
vim $home/getflow/runallofflineselect.sh
cd $home/getflow

./runallofflineselect.sh $1 $2 $3 $4 $5 $6
