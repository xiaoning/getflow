line_0-------------------------------------------------------------
Dataset: data18_hi.00100000.calibration_PCPEB.merge.AOD.ktest
Specs:-------------------------------------------------------------
run number: 100000
eta match range: 2.5
eta eff range: 2.5
pT cut: 1000.0
pT Truth cut: 1.0
data type: AOD
minimum primary: 0
-------------------------------------------------------------------
MCE220224.2_pnfs_18_hi_100000_ktest_MCEff_2.5_0.3_HITight_False
line_-------------------------------------------------------------
