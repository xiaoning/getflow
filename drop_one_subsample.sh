#!/bin/bash
vst=0
ved=1
version='FlowOL240817.1'
nversion='FlowOL240827.3'
dataset=data18mb0aod2cb
ndataset=data18mb0aod2cb
#${dest%"cb"}\all
nconfigs=1
input=/usatlas/u/cher97/getflow/txts/$dataset\runalloffline_runlist.txt

nsubs=10

st=0
ed=44

origin=/usatlas/u/cher97/output_files/local/offline_flow/offline_flow_$version\_$dataset
dest=/usatlas/u/cher97/output_files/local/offline_flow/offline_flow_$nversion\_$ndataset
mkdir -p $dest
#for ii in $(seq $loop1_st $loop1_ed); do
#    i=${list1[ii]}
linenumber=1
while IFS= read -r line <&3; do
    if [ $linenumber -ge $st ] && [ $linenumber -le $ed ]; then
        for i in $(seq 0 $((nconfigs - 1))); do
            folderi=$origin/offline_flow_$version\_$dataset\_$linenumber/config$i
            foldero=$dest/offline_flow_$nversion\_$ndataset\_$linenumber/config$i
            echo $foldero
            mkdir -p $foldero
            ind=0
            for j in $(seq 0 $((nsubs - 1))); do
                if [ $((linenumber % 10)) -eq $j ]; then
                    continue
                fi
                cp $folderi/sub$nsubs.$j.root $foldero/sub$((nsubs - 1)).$ind.root
                ind=$((ind + 1))
            done
            cd $foldero
            hadd -f -v 1 tots.root sub$((nsubs-1)).*.root
        done
    fi
    linenumber=$((linenumber + 1))
done 3<$input
