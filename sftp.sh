#!/bin/bash
#./runhadd_round1.sh $1 foldername:spmethod $2 tag $3 data $4:nsubs $5:do nfiles round $6: nconfigs $7:dest
input=/usatlas/u/cher97/getflow/txts/$3runall_runlist.txt
st=0
ed=37

if [ "$7" == "" ]; then
    master='/atlasgpfs01/usatlas/data/cher97'
else
    master='/pnfs/usatlas.bnl.gov/users/cher97'
fi
timestamp=$(date +%s)
linenumber=1

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

nconfigs=$6

while IFS= read -r line <&3; do
    echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
    folder=$1/$1_$2_$3_$linenumber
cd /usatlas/u/cher97/output_files/local/$1/
    if [ "$line" = "" ] || [ "$line" = "done" ]; then
        break
    fi
    if [ $linenumber -gt $st ] && [ $linenumber -le $ed ]; then
        sftp -r cher97@sftp.sdcc.bnl.gov:/atlasgpfs01/usatlas/data/cher97/output_files/local/$1/$1_$2_$3_$linenumber .
        cd $1_$2_$3_$linenumber/config0/
        hadd -f tots.root sub20.*.root
    fi
    linenumber=$((linenumber + 1))
done 3<$input
