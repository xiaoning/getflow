#!/bin/bash
#input="/atlasgpfs01/usatlas/data/cher97/mc16_5TeV.txt"
# ./runallloop.sh no1 process no2 foldername no3 inputtxt no4 nsubs no5 comb no6 configs no7 dest
# for large dataset, use pnfs for storage

input=~/getflow/txts/$3.txt
#input="mc16_5TeV_short.txt"
dest=$7
mkdir -p $dest/$2

#indexline=$1
linenumber=0
comb=$5
process=$1
nsubs=$4
configs=$6
bg=$((comb * process))
ed=$((comb * (process + 1)))

tempdir=$(mktemp -d)
cd $tempdir
echo $tempdir
mkdir -p $tempdir/'tempin'$2_$bg

while IFS= read -r line; do
    if [ $bg -le $linenumber -a $ed -gt $linenumber ]; then
        echo $line
        xrdcp 'root://dcgftp.usatlas.bnl.gov:1096/'$line $tempdir/'tempin'$2_$bg
        #xrdcp $line $tempdir/'tempin'$2_$bg
    fi
    linenumber=$((linenumber + 1))
done <$input

cd $tempdir/'tempin'$2_$bg
filename=$(ls *.root*)
echo $filename

mkdir -p $tempdir/'tempout'$2_$bg
cd $tempdir/'tempout'$2_$bg
cp /usatlas/u/cher97/getflow/py/$2ATestRun_eljob.py $tempdir/'tempout'$2_$bg/ATestRun_eljob.py
sed -i "s@^alg.numSubs.*@alg.numSubs=$4@" $tempdir/'tempout'$2_$bg/ATestRun_eljob.py
sed -i "s@^inputFilePath = .*@inputFilePath = '$tempdir/tempin$2_$bg'@" $tempdir/'tempout'$2_$bg/ATestRun_eljob.py
# sed -i "s@^ROOT.SH.ScanDir().filePattern(.*@ROOT.SH.ScanDir().filePattern( '*root*').scan( sh, inputFilePath )@" $tempdir/'tempout'$2_$bg/ATestRun_eljob.py

#cp $tempdir/'tempout'$2_$bg/ATestRun_eljob.py $tempdir/'tempout'$2_$bg/ATestRun_eljob$4.$i.py
chmod +x $tempdir/'tempout'$2_$bg/ATestRun_eljob.py
mkdir -p $dest/$2/pys$nsubs
if [[ "$dest" == *"pnfs"* ]]; then
    xrdcp $tempdir/'tempout'$2_$bg/ATestRun_eljob.py 'root://dcgftp.usatlas.bnl.gov:1096/'$dest/$2/pys$nsubs/ATestRun_eljob$bg-$ed.py
else
    cp $tempdir/'tempout'$2_$bg/ATestRun_eljob.py $dest/$2/pys$nsubs/ATestRun_eljob$bg-$ed.py
fi
#sed -i "s@^alg.FileName.*@alg.FileName = \"mce_$1_$bg.$4.$i\"@" $tempdir/'tempout'$2_$bg/ATestRun_eljob$4.$i.py
#sed -i "s@^alg.Rem.*@alg.Rem = $i@" $tempdir/'tempout'$2_$bg/ATestRun_eljob$4.$i.py
#echo $PWD
$tempdir/'tempout'$2_$bg/ATestRun_eljob.py --submission-dir=submitDir
for ((j = 0; j < $configs; j++)); do
    for ((i = 0; i < $nsubs; i++)); do
        mkdir -p $dest/$2/config$j/sub$4.$i/
        ls $tempdir/tempout$2_$bg/submitDir/data-myOutput$j.$i/*.root
        if [[ "$dest" == *"pnfs"* ]]; then
            xrdcp $tempdir/tempout$2_$bg/submitDir/data-myOutput$j.$i/*.root $dest/$2/config$j/sub$4.$i/mce_$1_$bg-$ed.$4.$i'.root'
            mkdir -p $dest/$2/config$j/sub$4.$i/mce_$1_$bg-$ed.$4.$i.csv
            xrdcp $tempdir/tempout$2_$bg/eventnumber*.csv $dest/$2/config$j/sub$4.$i/mce_$1_$bg-$ed.$4.$i.csv/
            xrdcp $tempdir/tempout$2_$bg/submitDir/data-myFlow/*.root $dest/$2/config$j/sub$4.$i/flow_$1_$bg-$ed.$4.$i'.root'
        else
            cp $tempdir/tempout$2_$bg/submitDir/data-myOutput$j.$i/*.root $dest/$2/config$j/sub$4.$i/mce_$1_$bg-$ed.$4.$i'.root'
            mkdir -p $dest/$2/config$j/sub$4.$i/mce_$1_$bg-$ed.$4.$i.csv
            cp $tempdir/tempout$2_$bg/eventnumber*.csv $dest/$2/config$j/sub$4.$i/mce_$1_$bg-$ed.$4.$i.csv/
            cp $tempdir/tempout$2_$bg/submitDir/data-myFlow/*.root $dest/$2/config$j/sub$4.$i/flow_$1_$bg-$ed.$4.$i'.root'
        fi
        #cp $tempdir/'tempout'$2_$bg/mce_$1_$bg.$4.$i'.txt' /atlasgpfs01/usatlas/data/cher97/$2/sub$4.$i/mce_$1_$bg-$ed.$4.$i'.txt'
    done
done

sleep 2
#rm -rf $tempdir/tempin$2_$bg
#rm -rf $tempdir/tempout$2_$bg
#rm -rf $tempdir
