#!/bin/bash
#input="/atlasgpfs01/usatlas/data/cher97/mc16_5TeV.txt"
# ./runallloop.sh no1 process no2 foldername no3 inputtxt no4 nsubs no5 comb no6 configs no7 dest no8 home no9 outputdest
# for large dataset, use pnfs for storage

input=$8/getflow/txts/$3.txt
#input="mc16_5TeV_short.txt"
dest=$7
#outputdest=$9
#indexline=$1
linenumber=0
comb=$5
process=$1
nsubs=$4
configs=$6
bg=$((comb * process))
ed=$((comb * (process + 1)))

while IFS= read -r line; do
    if [ $bg -le $linenumber -a $ed -gt $linenumber ]; then
        echo $line
    fi
    if [ $ed -le $linenumber ]; then
        break
    fi
    linenumber=$((linenumber + 1))
done <$input

for ((j = 0; j < $configs; j++)); do
    for ((l = $bg; l < $linenumber; l++)); do
        if [[ $dest == *"pnfs"* ]]; then
            xrdcp /usatlas/scratch/cher97/$2/config$j/cpp/flow_$1_$bg-$linenumber.$4.$i'.cpp' root://dcgftp.usatlas.bnl.gov:1096/$dest/$2/config$j/cpp/flow_$1_$bg-$linenumber.$4.$i'.cpp'
        else
            cp $outfolder'/exec.cpp' $dest/$2/config$j/cpp/flow_$1_$bg-$linenumber.$4.$i'.cpp'
        fi

        #root -q -b 'exec.cpp("'$filename'", "'$output'")'
        #cat exec.cpp
    done
    if [[ $dest == *"pnfs"* ]]; then
        echo 'PNFS'
    else
        cat $outfolder/*output_*.csv >>$dest/$2/config$j/csv/flow_$1_$bg-$linenumber.$4.$i'.csv'
    fi
    for ((i = 0; i < $nsubs; i++)); do
        if [[ $dest == *"pnfs"* ]]; then
            echo 'copying /usatlas/scratch/cher97/'$2/config$j/sub$4.$i/flow_$1_$bg-$linenumber.$4.$i'.root to root://dcgftp.usatlas.bnl.gov:1096/'$dest/$2/config$j/sub$4.$i/flow_$1_$bg-$linenumber.$4.$i'.root'
            rm $dest/$2/config$j/sub$4.$i/flow_$1_$bg-$linenumber.$4.$i'.root'
            xrdcp /usatlas/scratch/cher97/$2/config$j/sub$4.$i/flow_$1_$bg-$linenumber.$4.$i'.root' root://dcgftp.usatlas.bnl.gov:1096/$dest/$2/config$j/sub$4.$i/flow_$1_$bg-$linenumber.$4.$i'.root'
        else
            cp output_$i.root $dest/$2/config$j/sub$4.$i/flow_$1_$bg-$linenumber.$4.$i'.root'
        fi
    done
    sleep 2
done

sleep 2
