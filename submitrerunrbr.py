import csv
import numpy as np
import sys

folder = sys.argv[1]
version = sys.argv[2]
dataset = sys.argv[3]
nsubs = sys.argv[4]
comb = int(sys.argv[5])
suffix = sys.argv[6]
linenumber = int(sys.argv[7])
runlist = []
filelist = []
filename = f"/atlasgpfs01/usatlas/data/cher97/output_files/local/{folder}/{folder}_{version}_{dataset}/{folder}_{version}_{dataset}_{linenumber}/err.{suffix}"
reader = csv.reader(open(filename, "rt"), delimiter="_")
x = np.array(list(reader)).astype(np.int32)
x = np.unique(x, axis=0)
for i in range(len(x)):
    runlist.append(int(x[i, 0]))
    filelist.append(int(int(x[i, 1]) / comb))
print(str(runlist).replace(',',' '))
print(str(filelist).replace(',',' '))
