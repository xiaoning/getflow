#!/bin/bash

input=~/getflow/txts/$1.txt
>~/getflow/txts/$1_rules.txt
while IFS= read -r line; do
    rucio list-rules $line >> ~/getflow/txts/$1_rules.txt
done <$input