#!/bin/bash
#input="/atlasgpfs01/usatlas/data/cher97/mc16_5TeV.txt"
# ./runallloop.sh no1 process# no2 foldername no3 nsubs no4 timestamp $5 nconfigs $6 output_name
# for large dataset, use pnfs for storage

nconfigs=$5
for k in $(seq 0 $((nconfigs - 1))); do
    dest=$2/config$k/sub$3.$1
    mkdir -p $dest/tots
ls $dest/$4*/round1_*.root
    hadd -f -v 1 $dest/tots/sub$3.$1.root $dest/$4*/round1_*.root

    output=$6/config$k
    mkdir -p $output
    cd
    cp $dest/tots/sub$3.$1.root $output/
done
