#!/bin/bash
#./runhadd_round2.sh $1 foldername:spmethod $2:nconfig $3:tag1 $4:tag2
if [ "$2" == "" ]; then
    nconfigs=1
else
    nconfigs=$2
fi
master=/atlasgpfs01/usatlas/data/cher97/output_files/local/
#echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
for k in $(seq 0 $((nconfigs - 1))); do
    dest=$master/$1/$1_$3_combined/config$k
    mkdir -p $dest

    hadd -f $dest/tots.root $master/$1/$1_$3_data18jet/config$k/tots.root $master/$1/$1_$4_data18mb/config$k/tots.root
done
