#!/bin/bash
#./runhadd_round2.sh $1 foldername:spmethod $2 tag $3 data $4:nsubs $5:timestamp $6: nconfigs
input=~/getflow/txts/$3runall_runlist.txt

linenumber=1
nsubs=$4
echo $nsubs
nconfigs=$6

while IFS= read -r line <&3; do
    echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
    folder=$1/$1_$2_$3_$linenumber

    if [ "$line" = "" ] || [ "$line" = "*done*" ]; then
        break
    fi

	if [ $linenumber -le 20 ]; then
linenumber=$((linenumber+1))
continue
fi

if [ $linenumber -gt 40 ]; then
break
fi

    cp ~/getflow/condors/run_temp.job ~/getflow/condors/$folder'_runhadd_round2.job'

    for j in $(seq 0 $((nsubs - 1))); do
        /usatlas/u/cher97/getflow/runhadd_round2loop.sh $j $folder $nsubs $5 $6
    done
    linenumber=$((linenumber + 1))
done 3<$input
