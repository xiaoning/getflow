#include <TROOT.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <dirent.h>
#include "TFile.h"
#include "TChain.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TApplication.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TSystem.h"
#include <algorithm>
#include "TStyle.h"
#include "TColor.h"
#include "TProfile.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include <math.h>

#include <TH2.h>
#include <TStyle.h>

/*#include "../atlasstyle-00-04-02/AtlasUtils.h"
#include "../atlasstyle-00-04-02/AtlasStyle.h"
#include "../atlasstyle-00-04-02/AtlasLabels.h"
#include "../atlasstyle-00-04-02/AtlasStyle.C"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "../atlasstyle-00-04-02/AtlasLabels.C"
#include "../atlasstyle-00-04-02/AtlasUtils.C"
#endif

#ifdef __CINT__
gROOT->LoadMacro("../atlasstyle-00-04-02/AtlasLabels.C");
gROOT->LoadMacro("../atlasstyle-00-04-02/AtlasUtils.C");
#endif
*/

#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/Flow.h"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/Flow_main.cpp"
#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/Flow.cpp"
#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/corr.cpp"
#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/inits.cpp"
#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/performances.cpp"
#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/Utils.cpp"
#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/setters.cpp"
#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/qn.cpp"
#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/evt_perf.cpp"
#include "/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/evt_reject.cpp"
#endif

#ifdef __CINT__
gROOT->LoadMacro("/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/Flow_main.cpp");
gROOT->LoadMacro("/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/Flow.cpp");
gROOT->LoadMacro("/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/corr.cpp");
gROOT->LoadMacro("/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/inits.cpp");
gROOT->LoadMacro("/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/performances.cpp");
gROOT->LoadMacro("/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/Utils.cpp");
gROOT->LoadMacro("/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/setters.cpp");
gROOT->LoadMacro("/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/qn.cpp");
gROOT->LoadMacro("/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/evt_perf.cpp");
gROOT->LoadMacro("/atlasgpfs01/usatlas/data/cher97/offline_flow/offline_flow_FlowOL240229.4_data18peb0cb/config0/cpp/offline_flow/evt_reject.cpp");
#endif

const std::string fin = "/usatlas/u/cher97/test/data18mb0aod2/365914/user.xiaoning.36723104._000001.myFlow.root";
const std::string fout = "aod21sigma";

const std::vector<double> pT_range_config = cms_pT_range_small;
const std::vector<double> ctr_range_config = cms_ctr_range_coarse;
const bool m_use_mult = false;
const std::vector<int> N_config = {4};
const std::vector<int> Qn_N_config = {2, 3, 4, 6, 8};
const int nsubs_config = 1;
const int datatype_id = 0;
const int m_weight_tag = 1; // 0 for uniform weight, 1 for actual weight, 2 for random
const TString m_version = "FlowOL231019.1";
const TString m_dataset = "data18mb0";
const int linenumber = 32;
const int config = 0;
const TString m_dest = "/atlasgpfs01/usatlas/data/cher97";
const TString m_wtversion = "FlowOL240105_1";
const TString m_rcversion = "FlowOL231103.1";
const int m_refeta_id = 0;
const int m_poieta_id = 0;
const double m_refptlow = 1.0;
const double m_refpthigh = 5.0;
const double m_zvtx_low = 0.;
const double m_zvtx_high = 50.;
const bool m_of = true;
const bool m_perf = true;
const bool m_corr = false;
const bool m_qn = false;
const bool m_qn_tree = false;
const bool m_event = false;
const bool m_evt_perf = true;
const bool m_evt_reject = true;

const bool m_rc = false;

const bool m_cov = false;

const int m_puop = 1111;
const int m_ootpsides = 11;
const TString m_ootpname = "240129_11sigma";
const TString m_ootpntname = "240129_11sigma";
const int m_itpsides = 1;
const TString m_itpname = "240219_31sigma";

const bool m_rem_only = false;
const int m_rem = 0;
const std::vector<bool> selections_config = {0, 1, 1, 1}; //"both hits in inner most tracker","d0 pull cut 3.0", "z0sintheta pull cut 3.0", "HITight"

void exec(std::string in, std::string out)
{
    gInterpreter->GenerateDictionary("vector<vector<double> >", "vector");
    gInterpreter->GenerateDictionary("vector<vector<int> >", "vector");
    gInterpreter->GenerateDictionary("vector<vector<double> >", "vector");

    std::cout << "Entering main" << std::endl;
    // make object
    Flow *flow = new Flow(fin, fout, datatype_id, 0, nsubs_config);
    // evt performance
    flow->setEvtPerf(m_evt_perf);
    // evt rejection
    flow->setEvtReject(m_evt_reject);
    // performance
    flow->setPerformance(m_perf);
    // Qn
    flow->setQn(m_qn);
    flow->setNsQn(Qn_N_config);

    // Qn tree
    flow->setQnTree(m_qn_tree);

    // correlators
    flow->setCorr(m_corr);
    flow->setNs_corr(N_config);
    flow->setOverflow(m_of);
    flow->setCov(m_cov);
    // recentering
    flow->setRC(m_rc);
    flow->setRcVersion(m_rcversion);
    // weight
    flow->setWeightTag(m_weight_tag);
    flow->setWtVersion(m_wtversion);
    flow->setTrkCorrection(false, false);
    flow->setMCCorrection(false);
    flow->setPrescaleCorrection(true);
    // event filter
    flow->setEventFilter(false);
    // kinematics
    flow->setRefPt(m_refptlow, m_refpthigh);
    flow->setRefEta(m_refeta_id);
    flow->setPoiEta(m_poieta_id);
    flow->setZVtx(m_zvtx_low, m_zvtx_high);
    flow->setpTRange(pT_range_config);
    flow->setctrRange(ctr_range_config, m_use_mult);

    // event detail
    flow->setDoEvent(m_event);
    flow->setStopAtN(100);
    flow->setStopAtNDirectory(Form("%s/offline_flow/offline_flow_%s_%s_%d/config%d/csv/", m_dest.Data(), m_version.Data(), m_dataset.Data(), linenumber, config));
    flow->setOOTPVersion(m_ootpname);
    flow->setITPVersion(m_itpname);
    flow->setOOTPNTVersion(m_ootpntname);
    flow->setOOTPMSides(m_ootpsides);
    flow->setITPMSides(m_itpsides);
    flow->setPUOP(m_puop);
    flow->setRem(m_rem);
    flow->setRemOnly(m_rem_only);

    // run configs
    // flow->setNMaxEvents(10);
    flow->setVerbose(0);

    // Run
    flow->initBranches();
    flow->initTrackSelections(selections_config);
    flow->Loop();
    // init output path
    // init histogram
    // fill histogram (loop)
    // output
    return;
}
