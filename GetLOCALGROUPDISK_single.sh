#!/bin/bash

#
# Created on Dec 26 2019
#
# Copyright (c) 2020 University of Illinois Urbana-Champaign
#
# Xiaoning Wang
#


#rucio list-file-replicas --rse BNL-OSG2_LOCALGROUPDISK data18_hi.00$1.calibration_PCPEB.merge.AOD.$2 > $1_PC_pnfs.txt
#rucio list-file-replicas --rse BNL-OSG2_LOCALGROUPDISK data18_hi.00$1.calibration_CCPEB.merge.AOD.$2 > $1_CC_pnfs.txt
#root -b -q -l 'get_to_root_pnfs.cpp("'$1'","PC")'
#root -b -q -l 'get_to_root_pnfs.cpp("'$1'","CC")'
>'txts/'$1'_pnfs.txt'
rucio list-file-replicas --rse BNL-OSG2_LOCALGROUPDISK $1 >>'txts/'$1'_pnfs.txt'
root -b -q -l 'get_to_root_pnfs.cpp("'$1'","","BNL-OSG2_LOCALGROUPDISK")'
