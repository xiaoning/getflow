#!/bin/bash
#./submitrerun.sh no1:location no2:MCE211203.1 no3:mc18jet no4: optional, subsampling no5:optional combine m files no6:optional n configs no7: optional partial no8:optional use pnfs
#copy and open to be edited steering macro
home=/usatlas/u/cher97
master=/atlasgpfs01/usatlas/data/cher97/output_files/local/
dest=$master/$1/$1\_$2\_$3

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

if [ "$5" == "" ]; then
    comb=1
else
    comb=$5
fi

if [ "$6" == "" ]; then
    configs=1
else
    configs=$6
fi

if [ "$7" == "" ]; then
    partial=1
else
    partial=$7
fi

dataset=$3

desterr=$dest/err.tmpd
>$desterr
for ((j = 0; j < $configs; j++)); do
    cd $dest/config$j
    sed -n "/Disk quota exceeded/p" err.allclean >>$desterr
done
sed -i "/cp\: failed to extend/d" $desterr
sed -i "/line /d" $desterr
sed -i "s/cp\: error writing ‘\/usatlas\/scratch\/.*\/$1\/$1\_$2\_$3\/$1\_$2\_$3\_//" $desterr
sed -i "s/config$j\/sub$nsubs\.0\/flow\_//" $desterr
sed -i "s/.$nsubs.0.root’: Disk quota exceeded//" $desterr
sed -i 's![^-]*$!!' $desterr
sed -i 's![^_]*$!!' $desterr
sed -i "s/-//" $desterr
sed -i "s/_//" $desterr
sed -i "s/\//_/" $desterr
sed -i "/^$/d" $desterr

tmp=$(python3 $home/getflow/submitrerun.py $1 $2 $3 $4 1 tmpd)
a=${tmp%'['*}
a=${a%']'*}
a=${a#*'['}
#echo $a
b=${tmp##*'['}
b=${b%']'*}
#echo $b
sed -i "s/^runlist=(.*/runlist=($a)/" $home/getflow/runallofflineselect.sh
sed -i "s/^list=(.*/list=($b)/" $home/getflow/runallofflineselect.sh
vim $home/getflow/runallofflineselect.sh
cd $home/getflow

./runallofflineselect.sh $1 $2 $3 $4 $5 $6
