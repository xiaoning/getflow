#!/bin/bash
#./runhadd_round1.sh $1 foldername:spmethod $2 tag $3 data $4:nsubs $5:do nfiles round $6: nconfigs $7:dest
input=~/getflow/txts/$3runalloffline_runlist.txt
if [ "$7" == "" ]; then
    master='/atlasgpfs01/usatlas/data/cher97'
else
    master='/pnfs/usatlas.bnl.gov/users/cher97'
fi
timestamp=$(date +%s)
linenumber=1

if [ "$4" == "" ]; then
    nsubs=1
else
    nsubs=$4
fi

nconfigs=$6

while IFS= read -r line <&3; do
    echo 'line_'${bold}$linenumber${normal}'-------------------------------------------------------------'
    folder=$1/$1_$2_$3_$linenumber

    if [ "$line" = "" ] || [ "$line" = "done" ]; then
        break
    fi
    nhadd=0
    cp ~/getflow/condors/run_temp.job ~/getflow/condors/$folder'_runhaddoffline_round1.job'

    # create list of files in txt
    for k in $(seq 0 $((nconfigs - 1))); do
        for j in $(seq 0 $((nsubs - 1))); do
            dest=$master/$folder/config$k/sub$nsubs.$j
            ls $dest/*_*.root >$dest/totalfiles.txt
            totn=$(wc -l <$dest/totalfiles.txt)
            nf=$5
            n=$(((totn + nf - 1) / nf))
            python /usatlas/u/cher97/getflow/splitfiles.py $dest totalfiles $5
            nhadd=$((nhadd + n))
        done
    done

    input_loc=/usatlas/u/cher97/getflow/txts/$folder/
    mkdir -p $input_loc
    ls $master/$folder/config*/sub$nsubs.*/totalfiles_*.txt >$input_loc/totalfiles_condor.txt

    sed -i "s@^Executable.*@Executable   = /usatlas/u/cher97/getflow/runhadd_round1loop.sh@" ~/getflow/condors/$folder'_runhaddoffline_round1.job'
    sed -i "s@^Arguments.*@Arguments       = \$(Process) $input_loc/totalfiles_condor.txt $timestamp@" ~/getflow/condors/$folder'_runhaddoffline_round1.job'
    sed -i "s@^Queue.*@Queue $nhadd@" ~/getflow/condors/$folder'_runhaddoffline_round1.job'

    condor_submit ~/getflow/condors/$folder'_runhaddoffline_round1.job'
    linenumber=$((linenumber + 1))
done 3<$input
