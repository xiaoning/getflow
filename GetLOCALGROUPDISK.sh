#!/bin/bash

#
# Created on Dec 26 2019
#
# Copyright (c) 2020 University of Illinois Urbana-Champaign
#
# Xiaoning Wang
#


input=txts/$1'.txt'

while IFS= read -r line; do
    line=${line//[[:blank:]]/}
    echo $line
    >'txts/'$line'_pnfs.txt'
    rucio list-file-replicas --rse BNL-OSG2_LOCALGROUPDISK $line >>'txts/'$line'_pnfs.txt'
    root -b -q -l 'get_to_root_pnfs.cpp("'$line'","","BNL-OSG2_LOCALGROUPDISK")'
done <$input
